<?php

    /**
     * @author Clark Tomlinson  <fallen013@gmail.com>
     * @since 1/29/14, 12:31 PM
     * @link http://www.clarkt.com
     * @copyright Clark Tomlinson © 2014
     *
     */
    class DiscoverController extends BaseController
    {
        protected $layout = 'layout/fullwidth';

        public function index()
        {
            $this->layout->content = View::make('discover')
                                         ->with(
                                             'states',
                                             State::with('colleges')
                                                  ->remember('5')
                                                  ->get()
                                         );
        }

        public function getSubmitSchool()
        {
            $this->layout = false;
            return View::make('modals.submitSchool');
        }

        public function postSubmitSchool()
        {
            $this->layout = false;

            if (SchoolRequests::where('school_name', '=', Input::get('schoolName'))
                              ->count()
            ) {
                return Response::json(
                    array(
                        'status'  => 'error',
                        'message' => 'This school has already been requested.'
                    )
                );
            }

            $request                 = new SchoolRequests();
            $request->school_name    = Input::get('schoolName');
            $request->school_website = Input::get('schoolWebsite');
            $request->save();
            return Response::json(
                array(
                    'status'  => 'success',
                    'message' => 'Thanks for your submission we will add this school as soon as possible!'
                )
            );
        }
    }