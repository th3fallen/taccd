<?php
    use Cartalyst\Sentry\Users\UserExistsException;

    /**
     * @author Clark Tomlinson  <fallen013@gmail.com>
     * @since 2/3/14, 3:38 PM
     * @link http://www.clarkt.com
     * @copyright Clark Tomlinson © 2014
     *
     */
    class AccountController extends BaseController
    {
        /**
         * @var string
         */
        protected $layout = 'layout.fullwidth';

        /**
         *
         */
        public function getPreferences()
        {
            $this->layout->content = View::make('account.preferences');
        }

        /**
         * @return \Illuminate\Http\JsonResponse
         */
        public function postPersonalInfo()
        {
            $user = Sentry::getUser();
            $user->first_name = Input::get('firstname');
            $user->last_name = Input::get('lastname');
            $user->college = College::findBySlug(Str::slug(Input::get('college')))->id;
            $user->save();

            return Response::json(
                           array(
                               'status'  => 'success',
                               'message' => 'Your Information has been updated'
                           )
            );
        }

        /**
         * @return \Illuminate\Http\JsonResponse
         */
        public function postAccountInfo()
        {

            $avatarType = Input::get('avatarProvider');

            try {
                $user = Sentry::getUser();
                $user->username = Input::get('username');
                $user->email = Input::get('email');
                $user->avatar_provider = $avatarType;


                if (Input::hasFile('avatar')) {
                    $file = Input::file('avatar');
                    if ($file->getSize() < '5242880') {
                        $fileParts = explode('.', $file->getClientOriginalName());

                        $fileName = md5(time() . '_' . $fileParts[0]) . '.' . end($fileParts);
                        $file->move(storage_path('avatars'), $fileName);
                        $user->avatar_url = $fileName;

                    } else {
                        return Response::json(
                                       array(
                                           'status'  => 'error',
                                           'message' => 'Your file cannot be larger than 5M'
                                       )
                        );
                    }
                } elseif ($avatarType != 'custom') {
                    File::delete(storage_path('avatars') . $user->avatar_url);
                    $user->avatar_url = '';
                }

                $user->save();
            } catch (UserExistsException $e) {
                return Response::json(
                               array(
                                   'status'  => 'error',
                                   'message' => 'That username is already in use'
                               )
                );
            }

            return Response::json(
                           array(
                               'status'  => 'success',
                               'message' => 'Your Information has been updated'
                           )
            );
        }

        /**
         * @return \Illuminate\Http\JsonResponse
         */
        public function postChangePassword()
        {
            $user = Sentry::getUser();
            if (!empty($user->access_token)) {
                $resetCode = $user->getResetPasswordCode();


                if ($user->checkResetPasswordCode($resetCode)) {
                    if ($user->attemptResetPassword($resetCode, Input::get('newpass'))) {

                        $user->access_token = '';
                        $user->save();
                        return Response::json(
                                       array(
                                           'status'  => 'success',
                                           'message' => 'Your Password has been changed'
                                       )
                        );
                    }
                    return Response::json(
                                   array(
                                       'status'  => 'error',
                                       'message' => 'There was an issue changing your password please try again.'
                                   )
                    );
                }
            }
            if ($user->checkPassword(Input::get('oldpass'))) {
                // do reset
                $resetCode = $user->getResetPasswordCode();

                if ($user->checkResetPasswordCode($resetCode)) {
                    if ($user->attemptResetPassword($resetCode, Input::get('newpass'))) {
                        return Response::json(
                                       array(
                                           'status'  => 'success',
                                           'message' => 'Your Password has been changed'
                                       )
                        );
                    }
                    return Response::json(
                                   array(
                                       'status'  => 'error',
                                       'message' => 'There was an issue changing your password please try again.'
                                   )
                    );
                }
            }
            return Response::json(
                           array(
                               'status'  => 'error',
                               'message' => 'Your old password is not correct!'
                           )
            );
        }

        /**
         * @return \Illuminate\Http\JsonResponse
         */
        public function postCloseAccount()
        {
            $user = Sentry::getUser();
            if ($user->checkPassword(Input::get('oldpass'))) {
                $user->delete();
                return Response::json(
                               array(
                                   'status'   => 'success',
                                   'message'  => '',
                                   'redirect' => URL::route('index')
                               )
                );
            }
            return Response::json(
                           array(
                               'status'  => 'error',
                               'message' => 'Your old password is not correct!'
                           )
            );
        }
    }