<?php

    /**
     * @author Clark Tomlinson  <fallen013@gmail.com>
     * @since 1/29/14, 12:31 PM
     * @link http://www.clarkt.com
     * @copyright Clark Tomlinson © 2014
     *
     */
    class CollegeController extends BaseController
    {
        /**
         * @var string
         */
        protected $layout = 'layout/colleges-temp';

        /**
         * @param $slug
         */
        public function index($slug)
        {
            $college = College::findBySlug($slug);

            View::share(
                'college',
                    $college
            );
            $this->layout->content = View::make('college');
        }

        /**
         * @return \Illuminate\Http\JsonResponse
         */
        public function likeCollege()
        {
            $this->layout = false;
            $collegeId = Input::get('id');
            $userid = Sentry::getUser()
                            ->getId();

            if (User::likesCollege($collegeId)) {
                CollegeLikes::where('college_id', '=', $collegeId)
                            ->where('account_id', '=', $userid)
                            ->delete();
                return Response::json(array('status' => 'success'));

            } else {
                $collegeLikes = new CollegeLikes();
                $collegeLikes->account_id = $userid;
                $collegeLikes->college_id = Input::get('id');
                $collegeLikes->save();
                return Response::json(array('status' => 'success'));

            }
        }


        /**
         * @return \Illuminate\Http\JsonResponse
         */
        public function likeConfession()
        {
            $this->layout = false;

            if (!Sentry::check()) {
                return Response::json(
                               array(
                                   'status'  => 'error',
                                   'message' => 'You must be logged in to taccd a post'
                               )
                );
            }

            $confessionId = Input::get('id');
            $userId = Sentry::getUser()
                            ->getId();
            if (User::likesConfession(Input::get('id'))) {
                ConfessionLikes::where('confession_id', '=', $confessionId)
                               ->where('account_id', '=', $userId)
                               ->delete();
                return Response::json(array('status' => 'success'));
            } else {
                $like = new ConfessionLikes();

                $like->account_id = Sentry::getUser()
                                          ->getId();
                $like->confession_id = Input::get('id');
                $like->save();
                return Response::json(array('status' => 'success'));
            }
        }

        /**
         * @internal param $query
         *
         * @return string
         */
        public function lookup()
        {
            $this->layout = false;
            $query = Input::get('query');

            return array(
                'suggestions' => College::where('name', 'like', "%$query%")
                                        ->limit(5)
                                        ->get(array('id as data', 'name as value'))
                                        ->toArray()
            );
        }

        /**
         * @return \Illuminate\View\View
         */
        public function getConfessionModal()
        {
            $this->layout = false;
            return View::make('modals.confession');
        }

        /**
         * @return \Illuminate\View\View
         */
        public function postConfessionComment()
        {
            $this->layout = false;
            $user = Sentry::getUser();

            if ($this->commentTimeout($user->getId())) {
                return Response::json(
                               array(
                                   'status'  => 'error',
                                   'message' => 'You may only post a comment every 30 seconds'
                               )
                );
            }

            if (empty(Input::get('confession'))) {
                return Response::json(
                               array(
                                   'status'  => 'error',
                                   'message' => 'You cannot post an empty comment'
                               )
                );
            }

            $comment = new ConfessionComments();
            $comment->confession_id = Input::get('confessionID');
            $comment->account_id = $user->getId();
            $comment->comment_text = Input::get('confession');
            $comment->save();

            return View::make('partials.ajaxComments')
                       ->with('comment', $comment)
                       ->with('confession', Confession::find(Input::get('confessionID')));
        }

        /**
         * @return string
         */
        public function postLoadMoreComments()
        {
            $this->layout = false;

            $skip = Input::get('offset');
            $comments = ConfessionComments::where('confession_id', '=', Input::get('confessionID'))
                                          ->skip($skip)
                                          ->limit(5)
                                          ->orderBy('created_at', 'desc')
                                          ->get();
            $commentBuild = array();

            foreach ($comments as $comment) {
                $commentBuild[] = View::make('partials.ajaxComments')
                                      ->with('confession', Confession::find(Input::get('confessionID')))
                                      ->with('comment', $comment)
                                      ->render();
            }


            return implode('', $commentBuild);
        }

        /**
         * @return \Illuminate\Http\JsonResponse
         */
        public function postConfessionModal()
        {
            $this->layout = false;
            $user = Sentry::getUser();

            if ($this->confessionTimeout($user->getId())) {
                return Response::json(
                               array(
                                   'status'  => 'error',
                                   'message' => 'You may only post a confession every 1 minute'
                               )
                );
            }

            $confession = new Confession();
            $confession->account_id = $user->getId();
            $confession->college_id = $user->college;
            $confession->confession_text = Input::get('confession');
            $confession->anonymous = Input::has('anonymous');
            $confession->save();

            return Response::json(
                           array(
                               'status'   => 'success',
                               'message'  => 'Confession Posted (Redirecting...)',
                               'redirect' => URL::route('college', array('slug' => Str::slug($user->colleges->name)))
                           )
            );
        }

        /**
         * @param $slug
         *
         * @return \Illuminate\View\View
         */
        public function getArtworkRequest($slug)
        {
            $this->layout = false;
            return View::make('modals.submitArtwork')
                       ->with('slug', $slug);

        }

        /**
         * @return \Illuminate\Http\JsonResponse
         */
        public function postArtworkRequest()
        {
            $this->layout = false;

            $school = College::findBySlug(Input::get('school_slug'));

            $request = new SchoolArtworkRequests();
            $request->school_id = $school->id;
            $request->requestLink = Input::get('schoolArt');
            $request->user_id = Sentry::getUser()
                                      ->getId();
            $request->status = '0';
            $request->save();

            return Response::json(
                           array(
                               'status'  => 'success',
                               'message' => 'Thank you for your submission we will review it and be in touch.'
                           )
            );

        }

        /**
         * @param $slug
         *
         * @return array
         */
        public function loadConfessions($slug)
        {
            $this->layout = false;
            $offset = Input::get('offset');
            $confessions = College::findBySlug($slug)
                                  ->confessions()
                                  ->skip($offset)
                                  ->limit(10)
                                  ->get();
            $template = array();
            foreach ($confessions as $confession) {
                $template[] = $this->loadConfessionTemplate($confession)
                                   ->render();
            }
            return $template;

        }

        /**
         * @return array
         */
        public function loadDashboardConfessions()
        {

            $this->layout = false;
            $offset = Input::get('offset');
            $collegeIds = Sentry::getUser()
                                ->collegeLikes()
                                ->get(array('college_id'))
                                ->toArray();

            $collegeIds = array_flatten($collegeIds);

            $confessions = Confession::whereIn('college_id', $collegeIds)
                                     ->orderBy('id', 'desc')
                                     ->orderBy('created_at', 'desc')
                                     ->skip($offset)
                                     ->limit(10)
                                     ->get();

            $template = array();
            foreach ($confessions as $confession) {
                $template[] = $this->loadConfessionTemplate($confession)
                                   ->render();
            }
            return $template;

        }


        /**
         * @param $confession
         *
         * @return \Illuminate\View\View
         */
        private function loadConfessionTemplate($confession)
        {
            $this->layout = false;

            return View::make('loops.confessions')
                       ->with('confession', $confession);
        }

        /**
         * @param $userId
         *
         * @return bool
         */
        private function commentTimeout($userId)
        {
            $comments = ConfessionComments::where('account_id', '=', $userId)
                                          ->orderBy('created_at', 'desc')
                                          ->first(array('created_at'));

            if (empty($comments) || $comments->created_at->diffInSeconds() > '30') {
                return false;
            }
            return true;
        }

        /**
         * @param $userId
         *
         * @return bool
         */
        private function confessionTimeout($userId)
        {
            $confessions = Confession::where('account_id', '=', $userId)
                                     ->orderBy('created_at', 'desc')
                                     ->first(array('created_at'));

            if (empty($confessions) || $confessions->created_at->diffInMinutes() > '1') {
                return false;
            }
            return true;
        }

    }