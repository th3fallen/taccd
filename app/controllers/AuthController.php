<?php
    use Illuminate\Mail\Message;

    /**
     * @author Clark Tomlinson  <fallen013@gmail.com>
     * @since 2/3/14, 3:38 PM
     * @link http://www.clarkt.com
     * @copyright Clark Tomlinson © 2014
     *
     */
    class AuthController extends Controller
    {


        /**
         * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
         */
        public function getLogin()
        {
            if (Sentry::check()) {
                return Redirect::route('index');
            }

            return View::make('auth.login');
        }

        /**
         *
         */
        public function getLoginFacebook()
        {
            $facebook = new Facebook(Config::get('facebook'));
            $params = array(
                'redirect_uri' => URL::route('login.facebook.callback'),
                'scope'        => 'email'
            );
            return Redirect::to($facebook->getLoginUrl($params));
        }

        /**
         *
         */
        public function getLoginFacebookCallback()
        {
            $code = Input::get('code');
            if (Str::length($code) == 0) {
                return Redirect::route('index')
                               ->with('message', 'There was an error communicating with Facebook');
            }

            $facebook = new Facebook(Config::get('facebook'));
            $uid = $facebook->getUser();

            if ($uid == 0) {
                return Redirect::route('index')
                               ->with('message', 'There was an error');
            }

            $me = $facebook->api('/me');

//            dd($me);

            // If the user has already registered with facebook log them in and be done
            if (User::where('uid', '=', $me['id'])
                    ->count()
            ) {
                Sentry::login(
                      User::where('uid', '=', $me['id'])
                          ->first()
                );
                return Redirect::route('index');
            }

            // First lets check and verify this user is not already registered
            if (empty(User::where('email', '=', $me['email'])
                          ->count())
            ) {
                // Lets add a new user
                $user = Sentry::register(
                              array(
                                  'first_name'      => $me['first_name'],
                                  'last_name'       => $me['last_name'],
                                  'email'           => $me['email'],
                                  'username'        => $me['id'],
                                  'password'        => Str::random(10),
                                  'avatar_provider' => 'custom',
                                  'avatar_url'      => 'https://graph.facebook.com/' . $me['username'] . '/picture?type=large',
                                  'access_token'    => $facebook->getAccessToken(),
                                  'uid'             => $me['id'],
                                  'referral_code'   => Str::random()
                              ),
                                  true
                );
                // Dont forget to set their group
                $studentGroup = Sentry::findGroupByName('Student');
                $user->addGroup($studentGroup);

                Sentry::login($user);
                return Redirect::route('index');
            } else {
                User::where('email', '=', $me['email'])
                    ->update(array('uid' => $me['id']));


                Sentry::login(
                      User::where('email', '=', $me)
                          ->first()
                );
                return Redirect::route('index');
            }
            return Redirect::route('index')
                           ->with('message', 'That email address already has an account');
        }

        /**
         * @return \Illuminate\Http\JsonResponse
         */
        public function postLogin()
        {
            try {

                $credentials = array(
                    'username' => Input::get('username'),
                    'password' => Input::get('password')
                );

                $user = Sentry::authenticate($credentials, Input::get('remember_me'));

                if ($user) {
                    return Response::json(
                                   array(
                                       'status'   => 'success',
                                       'message'  => "Welcome Back {$user->fullName()}!",
                                       'redirect' => 'refresh'
                                   )
                    );
//                    return Redirect::route('index');
                }
            } catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
                echo 'User field required';
            } catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
                echo 'password not set';
            } catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
                return Response::json(
                               array(
                                   'status'  => 'error',
                                   'message' => 'Incorrect username or password'
                               )
                );

            } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
//                echo 'User was not found.';

                // If the user is not found we need to check against the legacy users so we can upgrade them
                $oldUser = LegacyUser::where('username', '=', Input::get('username'))
                                     ->first();

                if (isset($oldUser) && $oldUser->password == hashPass(
                        Input::get('username'),
                        Input::get('password'),
                        $oldUser->last_login
                    )
                ) {
                    $newUser = Sentry::createUser(
                                     array(
                                         'id'              => $oldUser->id,
                                         'username'        => $oldUser->username,
                                         'password'        => Input::get('password'),
                                         'email'           => $oldUser->email,
                                         'activated'       => true,
                                         'first_name'      => $oldUser->first_name,
                                         'last_name'       => $oldUser->last_name,
                                         'avatar_provider' => (empty($oldUser->avatar_url)) ? 'gravatar' : 'custom',
                                         'avatar_url'      => ($oldUser->avatar_url == 'avatars/default2.png') ? '' : $oldUser->avatar_url,
                                         'college'         => $oldUser->college_id,
                                         'referral_code'   => md5(Input::get('email') . Input::get('username')),
                                         'referrer_id'     => User::findByReferralCode(Input::get('referral_code'))->id

                                     )
                    );

                    $studentGroup = Sentry::findGroupByName('Student');
                    $newUser->addGroup($studentGroup);

                    Sentry::login($newUser);
                    // Remove old user from database to prevent duplicated user counts
                    $oldUser->delete();
                    return Response::json(
                                   array(
                                       'status'   => 'success',
                                       'message'  => "Welcome Back {$newUser->fullName()}!",
                                       'redirect' => URL::route('index')
                                   )
                    );

                }
                return Response::json(
                               array(
                                   'status'  => 'error',
                                   'message' => 'Incorrect Username or Password'
                               )
                );
            } catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
                return Response::json(
                               array(
                                   'status'  => 'error',
                                   'message' => 'User Not Acivated'
                               )
                );
            } // The following is only required if throttle is enabled
            catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e) {
                return Response::json(
                               array(
                                   'status'  => 'error',
                                   'message' => 'User is suspended'
                               )
                );
            } catch (Cartalyst\Sentry\Throttling\UserBannedException $e) {
                return Response::json(
                               array(
                                   'status'  => 'error',
                                   'message' => 'User is banned'
                               )
                );
            }
            return Response::json(
                           array(
                               'status'  => 'error',
                               'message' => 'There was an error logging you in please try again.'
                           )
            );
        }

        /**
         * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
         */
        public function getRegister()
        {
            if (Sentry::check()) {
                return Redirect::route('index');
            }

            return View::make('auth.register');
        }

        /**
         * @return \Illuminate\Http\JsonResponse
         */
        public function postRegister()
        {
            // First we need to check if the email address has already been registered
            if (User::where('email', '=', Input::get('email'))
                    ->count('id')
            ) {
                return Response::json(
                               array(
                                   'status'  => 'error',
                                   'message' => 'That email address has already been registered!'
                               )
                );
            }
            try {
                $user = Sentry::register(
                              array(
                                  'username'        => Input::get('username'),
                                  'email'           => Input::get('email'),
                                  'password'        => Input::get('password'),
                                  'avatar_provider' => 'gravatar',
                                  'referral_code'   => md5(Input::get('email') . Input::get('username')),
                                  'referrer_id'     => User::findByReferralCode(Input::get('referral_code'))
                              )
                );

                // Lets get the activation code
                $activationCode = $user->getActivationCode();
                $data = array(
                    'activationCode' => $activationCode,
                    'username'       => Input::get('username')
                );

                // And now lets send the code off to the user
                Mail::send(
                    'emails.auth.activation',
                        $data,
                        function (Message $message) {
                            $message->from('no-reply@taccd.com', 'Taccd');
                            $message->to(Input::get('email'));
                        }
                );

                // Dont forget to set their group
                $studentGroup = Sentry::findGroupByName('Student');
                $user->addGroup($studentGroup);


                return Response::json(
                               array(
                                   'status'  => 'success',
                                   'message' => 'You where successfully registered please check your email for your activation code!'
                               )
                );

            } catch (Cartalyst\Sentry\Users\UserExistsException $e) {
                return Response::json(
                               array(
                                   'status'  => 'error',
                                   'message' => 'That username is already taken'
                               )
                );
            }
        }

        public function activateUser($activationKey)
        {
            $user = User::where('activation_code', '=', $activationKey);
            if (!$user->count()) {
                return Redirect::route('index')
                               ->with('message', 'Invalid Activation Code');
            }
            $userInfo = $user->first();

            // Lets attempt to activate the user

            $userInfo->attemptActivation($activationKey);

            // Log user in
            Sentry::login($userInfo);

            Redirect::route('index');
        }

        /**
         * @return \Illuminate\View\View
         */
        public function getforgotPassword()
        {
            return View::make('auth.forgotPassword');
        }

        /**
         * @return \Illuminate\Http\JsonResponse
         */
        public function postforgotPassword()
        {
            try {
                $user = Sentry::findUserByLogin(Input::get('username'));

                $resetCode = $user->getResetPasswordCode();

                $data = array(
                    'resetCode' => $resetCode,
                    'username'  => $user->username
                );

                // And now lets send the code off to the user
                Mail::send(
                    'emails.auth.reminder',
                        $data,
                        function (Message $message) use ($user) {
                            $message->from('no-reply@taccd.com', 'Taccd');
                            $message->to($user->email);
                        }
                );

                return Response::json(
                               array(
                                   'status'  => 'success',
                                   'message' => 'Please check your inbox for further instructions.'
                               )
                );

            } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
                return Response::json(
                               array(
                                   'status'  => 'error',
                                   'message' => 'That email address was not found'
                               )
                );
            }
        }

        /**
         * @return \Illuminate\Http\RedirectResponse
         */
        public function getLogout()
        {
            Sentry::logout();
            return Redirect::route('index');
        }

    }