<?php

    class HomeController extends BaseController
    {

        /*
        |--------------------------------------------------------------------------
        | Default Home Controller
        |--------------------------------------------------------------------------
        |
        | You may wish to use controllers instead of, or in addition to, Closure
        | based routes. That's great! Here is an example controller method to
        | get you started. To route to this controller, just add the route:
        |
        |	Route::get('/', 'HomeController@showWelcome');
        |
        */

        protected $layout = 'layout/fullwidth';

        public function index()
        {
            if (Sentry::check()) {
                return Redirect::route('dashboard');
            }
            $this->layout->content = View::make('index')
                                         ->with(
                                         'colleges',
                                             College::remember('5')
                                                    ->count('id')
                )
                                         ->with(
                                         'users',
                                             User::remember('5')
                                                 ->count('id') + LegacyUser::remember(5)
                                                                           ->count('id')
                )
                                         ->with(
                                         'confessions',
                                             Confession::remember('5')
                                                       ->count('id')
                )
                                         ->with(
                                         'comments',
                                             ConfessionComments::remember('5')
                                                               ->count('id')
                )
                                         ->with(
                                         'likes',
                                             ConfessionLikes::remember(5)
                                                            ->count('confession_id')
                );
        }

        public function dashboard()
        {
            $this->layout = false;
            return View::make('dashboard')
                       ->with('user', Sentry::getUser())
                       ->with(
                       'colleges',
                           College::remember('5')
                                  ->count('id')
                )
                       ->with(
                       'users',
                           User::remember('5')
                               ->count('id') + LegacyUser::remember(5)
                                                         ->count('id')
                )
                       ->with(
                       'confessions',
                           Confession::remember('5')
                                     ->count('id')
                )
                       ->with(
                       'comments',
                           ConfessionComments::remember('5')
                                             ->count('id')
                )
                       ->with(
                       'likes',
                           ConfessionLikes::remember(5)
                                          ->count('confession_id')
                );

        }

        public function updates()
        {
            $this->layout->content = View::make('pages.updates')
                                         ->with(
                                         'updates',
                                             Updates::orderBy('created_at', 'desc')
                                                    ->where('visible', '=', '1')
                                                    ->get()
                );
        }

    }