<?php

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateGroups extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            // Create Groups

            Sentry::createGroup(
                  array(
                      'name'        => 'Administrator',
                      'permissions' => array(
                          'superuser' => 1
                      )
                  )
            )
                  ->save();

            Sentry::createGroup(
                  array(
                      'name'        => 'Student',
                      'permissions' => array(
                          'students' => 1
                      )
                  )
            )
                  ->save();

        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            //
            $administrator = Sentry::findGroupByName('Administrator')
                                   ->delete();


            $students = Sentry::findGroupByName('Student')
                              ->delete();
        }

    }
