<?php

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class FacebookLogin extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            // Add access token column to user table
            Schema::table(
                  'users',
                      function (Blueprint $table) {
                          $table->string('access_token')
                                ->nullable();
                          $table->string('uid')
                                ->nullable();
                      }
            );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table(
                  'users',
                      function (Blueprint $table) {
                          $table->dropColumn('access_token');
                          $table->dropColumn('uid');
                      }
            );
        }

    }
