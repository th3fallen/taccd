<?php

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class ArtworkRequest extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            // Create Artwork Request Table
            Schema::create(
                  'artwork_requests',
                      function (Blueprint $table) {
                          $table->increments('id');
                          $table->timestamps();
                          $table->integer('status');
                          $table->string('requestLink');
                          $table->integer('school_id');
                          $table->integer('user_id');
                      }
            );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            // Drop Table
            Schema::dropIfExists('artwork_requests');
        }

    }
