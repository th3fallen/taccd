<?php

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class AddUpdatesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            // Create updates table
            Schema::create(
                  'updates',
                      function (Blueprint $table) {
                          $table->increments('id');
                          $table->string('title');
                          $table->longText('body');
                          $table->boolean('visible');
                          $table->timestamps();
                      }
            );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            // drop updates table
            Schema::drop('updates');
        }

    }
