<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RequestedSchools extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create Table
        Schema::create('school_requests',
            function (Blueprint $table) {
                $table->increments('id');
                $table->text('school_name');
                $table->text('school_website');
                $table->timestamps();
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Drop Table
        Schema::drop('school_requests');
	}

}
