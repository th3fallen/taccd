<?php

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class UpdateEdits extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            // Handle all database edits nessesary for update to version 1.1
            Schema::table(
                  'accounts',
                      function (Blueprint $table) {
                          $table->renameColumn('email_address', 'email');
                          $table->dropColumn('email_valid');
                      }
            );

            Schema::rename('college_likes_log', 'college_likes');

            Schema::table(
                  'college_likes',
                      function (Blueprint $table) {
                          $table->index(array('account_id', 'college_id'));
                          $table->renameColumn('creation_date', 'created_at');
                          $table->dateTime('updated_at');

                      }
            );

            Schema::table(
                  'colleges',
                      function (Blueprint $table) {
                          $table->dropColumn('slug_name');
                          $table->renameColumn('creation_date', 'created_at');
                          $table->dateTime('updated_at');
                          $table->index('name');
                      }
            );

            Schema::table(
                  'confession_comments',
                      function (Blueprint $table) {
                          $table->renameColumn('creation_date', 'created_at');
                          $table->dateTime('updated_at');
                          $table->index(array('confession_id', 'account_id'));
                      }
            );

            Schema::rename('confession_likes_log', 'confession_likes');


            Schema::table(
                  'confession_likes',
                      function (Blueprint $table) {
                          $table->index(array('account_id', 'confession_id'));
                          $table->renameColumn('creation_date', 'created_at');
                          $table->dateTime('updated_at');
                      }
            );

            Schema::table(
                  'confessions',
                      function (Blueprint $table) {
                          $table->renameColumn('creation_date', 'created_at');
                          $table->dateTime('updated_at');
                          $table->index(array('account_id', 'college_id'));
                      }
            );

            DB::table('confessions')
              ->where('anonymous', '=', '0')
              ->update(array('anonymous' => '1'));

            Schema::create(
                  'users',
                      function (Blueprint $table) {
                          $table->increments('id');
                          $table->string('username');
                          $table->string('email');
                          $table->string('password');
                          $table->string('avatar_provider')
                                ->nullable();
                          $table->string('avatar_url')
                                ->nullable();
                          $table->integer('college')
                                ->nullable();
                          $table->text('permissions')
                                ->nullable();
                          $table->boolean('activated')
                                ->default(0);
                          $table->string('activation_code')
                                ->nullable();
                          $table->timestamp('activated_at')
                                ->nullable();
                          $table->timestamp('last_login')
                                ->nullable();
                          $table->string('persist_code')
                                ->nullable();
                          $table->string('reset_password_code')
                                ->nullable();
                          $table->string('first_name')
                                ->nullable();
                          $table->string('last_name')
                                ->nullable();
                          $table->timestamps();

                          // We'll need to ensure that MySQL uses the InnoDB engine to
                          // support the indexes, other engines aren't affected.
                          $table->engine = 'InnoDB';
                          $table->unique('email');
                          $table->index('activation_code');
                          $table->index('reset_password_code');
                      }
            );

            Schema::create(
                  'groups',
                      function (Blueprint $table) {
                          $table->increments('id');
                          $table->string('name');
                          $table->text('permissions')
                                ->nullable();
                          $table->timestamps();

                          // We'll need to ensure that MySQL uses the InnoDB engine to
                          // support the indexes, other engines aren't affected.
                          $table->engine = 'InnoDB';
                          $table->unique('name');
                      }
            );

            Schema::create(
                  'users_groups',
                      function (Blueprint $table) {
                          $table->integer('user_id')
                                ->unsigned();
                          $table->integer('group_id')
                                ->unsigned();

                          // We'll need to ensure that MySQL uses the InnoDB engine to
                          // support the indexes, other engines aren't affected.
                          $table->engine = 'InnoDB';
                          $table->primary(array('user_id', 'group_id'));
                      }
            );

            Schema::create(
                  'throttle',
                      function (Blueprint $table) {
                          $table->increments('id');
                          $table->integer('user_id')
                                ->unsigned();
                          $table->string('ip_address')
                                ->nullable();
                          $table->integer('attempts')
                                ->default(0);
                          $table->boolean('suspended')
                                ->default(0);
                          $table->boolean('banned')
                                ->default(0);
                          $table->timestamp('last_attempt_at')
                                ->nullable();
                          $table->timestamp('suspended_at')
                                ->nullable();
                          $table->timestamp('banned_at')
                                ->nullable();

                          // We'll need to ensure that MySQL uses the InnoDB engine to
                          // support the indexes, other engines aren't affected.
                          $table->engine = 'InnoDB';
                          $table->index('user_id');
                      }
            );

            // Custom modifications

            DB::statement(
              "UPDATE accounts
                            SET avatar_url = REPLACE (
                                avatar_url,
                                'images/avatars/',
                                '')"
            );

            DB::statement(
              "UPDATE colleges
                            SET avatar_url = REPLACE (
                                avatar_url,
                                'images/colleges/',
                                '')"
            );

            $increment = DB::table('accounts')
                           ->count('id');
            $increment = $increment + 1;

            DB::statement("ALTER TABLE `users` AUTO_INCREMENT=$increment");
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            //
        }

    }
