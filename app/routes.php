<?php

    /*
    |--------------------------------------------------------------------------
    | Application Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register all of the routes for an application.
    | It's a breeze. Simply tell Laravel the URIs it should respond to
    | and give it the Closure to execute when that URI is requested.
    |
    */

    /**
     * Page Routes
     */

    Route::get('/', array('as' => 'index', 'uses' => 'HomeController@index'));

    Route::get('discover', array('as' => 'discover', 'uses' => 'DiscoverController@index'));

    Route::get('discover/submitSchool', array('uses' => 'DiscoverController@getSubmitSchool'));

    Route::get(
         'dashboard',
             array('as' => 'dashboard', 'uses' => 'HomeController@dashboard', 'before' => 'auth')
    );


    Route::post(
         'college/loadDashboardConfessions',
             array('as' => 'college.dashboardConfessions', 'uses' => 'CollegeController@loadDashboardConfessions')
    );
    Route::post(
         'college/loadMoreComments',
             array('as' => 'college.dashboardConfessions', 'uses' => 'CollegeController@postLoadMoreComments')
    );

    Route::get('college/{slug}', array('as' => 'college', 'uses' => 'CollegeController@index'))
         ->where('slug', '[a-z-]+');

    Route::post(
         'college/{slug}/loadConfessions',
             array('as' => 'college.confessions', 'uses' => 'CollegeController@loadConfessions')
    )
         ->where('slug', '[a-z-]+');


    Route::post(
         'college/lookup',
             array('as' => 'college.lookup', 'uses' => 'CollegeController@lookup')
    );

    Route::get(
         'college/{slug}/getArtworkRequest',
             array('uses' => 'CollegeController@getArtworkRequest')
    )
         ->where('slug', '[a-z-]+');


    Route::get(
         'confession',
             array(
                 'as'   => 'confession',
                 'uses' => 'CollegeController@getConfessionModal'
             )
    );

    Route::get('updates', array('as' => 'updates', 'uses' => 'HomeController@updates'));

    /**
     * Legal Actions
     */

    Route::get(
         'terms',
             array(
                 'as' => 'terms',
                 function () {
                     return View::make('legal.terms');
                 }
             )
    );
    Route::get(
         'privacy',
             array(
                 'as' => 'privacy',
                 function () {
                     return View::make('legal.privacy');
                 }
             )
    );

    /**
     * Handle Any actions that require you be logged in
     * via ajax
     *
     */
    Route::group(
         array('before' => 'auth.ajax'),
             function () {


                 Route::post(
                      'confession',
                          array(
                              'as'   => 'confession.post',
                              'uses' => 'CollegeController@postConfessionModal'
                          )
                 );

                 Route::post(
                      'comment',
                          array(
                              'as'   => 'comment.post',
                              'uses' => 'CollegeController@postConfessionComment'
                          )
                 );

                 Route::post(
                      'college/getArtworkRequest',
                          array('as' => 'submitArtwork.post', 'uses' => 'CollegeController@postArtworkRequest')
                 );

                 Route::post(
                      'college/{slug}/likeConfession',
                          array('as' => 'college.like.confession', 'uses' => 'CollegeController@likeConfession')
                 )
                      ->where('slug', '[a-z-]+');

                 Route::post(
                      'college/{slug}/likeCollege',
                          array('as' => 'college.like', 'uses' => 'CollegeController@likeCollege')
                 )
                      ->where('slug', '[a-z-]+');


                 Route::post(
                      'discover/submitSchool',
                          array('as' => 'submitSchool.post', 'uses' => 'DiscoverController@postSubmitSchool')
                 );
             }
    );

    /**
     * Handle Auth Actions
     */
    Route::group(
         array('prefix' => 'account'),
             function () {
                 Route::get(
                      'activate/{activationkey}',
                          array('as' => 'login.activate', 'uses' => 'AuthController@activateUser')
                 );

                 Route::get(
                      'login',
                          array('as' => 'login.get', 'uses' => 'AuthController@getLogin')
                 );

                 Route::post(
                      'login',
                          array('as' => 'login.post', 'uses' => 'AuthController@postLogin')
                 );

                 Route::get(
                      'login/facebook',
                          array('as' => 'login.facebook', 'uses' => 'AuthController@getLoginFacebook')
                 );

                 Route::get(
                      'login/facebook/callback',
                          array('as' => 'login.facebook.callback', 'uses' => 'AuthController@getLoginFacebookCallback')
                 );

                 Route::get(
                      'logout',
                          array('as' => 'logout', 'uses' => 'AuthController@getLogout')
                 );

                 Route::get(
                      'register',
                          array('as' => 'register.get', 'uses' => 'AuthController@getRegister')
                 );

                 Route::post(
                      'register',
                          array('as' => 'register.post', 'uses' => 'AuthController@postRegister')
                 );

                 Route::get(
                      'login/facebook',
                          array('as' => 'login.facebook', 'uses' => 'AuthController@getLoginFacebook')
                 );

                 Route::get(
                      'login/facebook/callback',
                          array('as' => 'login.facebook.callback', 'uses' => 'AuthController@getLoginFacebookCallback')
                 );

                 Route::get(
                      'password/reset',
                          array('as' => 'passwordReset', 'uses' => 'AuthController@getResetPassword')
                 );

                 Route::get(
                      'forgotPassword',
                          array('as' => 'forgotPassword', 'uses' => 'AuthController@getForgotPassword')
                 );
                 Route::post(
                      'forgotPassword',
                          array('as' => 'forgotPassword.post', 'uses' => 'AuthController@postForgotPassword')
                 );
             }
    );

    /**
     * Handle routes that require authentication to enter
     */

    Route::group(
         array('prefix' => 'account', 'before' => 'auth'),
             function () {

                 Route::get(
                      'preferences',
                          array('as' => 'account.preferences', 'uses' => 'AccountController@getPreferences')
                 );

                 Route::post(
                      'personalInfo',
                          array('as' => 'account.postPersonalInfo', 'uses' => 'AccountController@postPersonalInfo')
                 );

                 Route::post(
                      'accountInfo',
                          array('as' => 'account.postAccountInfo', 'uses' => 'AccountController@postAccountInfo')
                 );

                 Route::post(
                      'changePassword',
                          array('as' => 'account.postChangePassword', 'uses' => 'AccountController@postChangePassword')
                 );

                 Route::post(
                      'closeAccount',
                          array('as' => 'account.postCloseAccount', 'uses' => 'AccountController@postCloseAccount')
                 );
             }
    );