<?php
    /**
     * @author Clark Tomlinson  <fallen013@gmail.com>
     * @since 2/3/14, 3:20 PM
     * @link http://www.clarkt.com
     * @copyright Clark Tomlinson © 2014
     *
     */

    /**
     * Legacy Password Hasher
     * @param $username
     * @param $password
     * @param $timestamp
     *
     * @return string
     */
    function hashPass($username, $password, $timestamp)
    {

        //### Modify timestamp for hashing
        $timestamp = strrev($timestamp);
        $timestamp = preg_replace('/[^0-9]/', '', $timestamp);
        $timestamp = md5($timestamp);
        $timestamp = substr($timestamp, 0, 22);

        //### Set up salt for hashing
        $salt = '$2y$10$' . $timestamp;

        //### Return the hashed password
        return crypt((strtolower($username) . $password), $salt);

    }