<?php
    /**
     * @author Clark Tomlinson  <fallen013@gmail.com>
     * @since 1/29/14, 10:47 AM
     * @link http://www.clarkt.com
     * @copyright Clark Tomlinson © 2014
     *
     */
    if (!function_exists('image')) {
        function image($name)
        {
            return route('assets', array('path' => $name));

        }
    }