<?php

    namespace App\Modules\Admin\Controllers;

    use DB;
    use Redirect;
    use Input;
    use Response;
    use Request;
    use Sentry;
    use Str;
    use User;
    use View;

    class UsersController extends \BaseController
    {

        protected $layout = 'admin::layout/master';


        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {

            if (Request::ajax()) {
                $this->layout = false;
                return Response::json(
                               array(
                                   'aaData' => \User::all()
                                                    ->toArray()
                               )
                );
            }
            //
            $this->layout->content = View::make('admin::pages.users.index')
                                         ->with(
                                         'users',
                                             \User::all()
                );

        }

        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function create()
        {
            //
            $this->layout->content = View::make('admin::pages.users.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function store()
        {
            //
            $update = new \User();
            $update->title = Input::get('title');
            $update->body = Input::get('body');
            $update->visible = Input::get('visible');
            $update->save();

            $this->layout->content = View::make('admin::pages.users.edit')
                                         ->with('update', $update);
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function show($id)
        {
            //
            $this->layout->content = View::make('admin::pages.users.edit')
                                         ->with('user', Sentry::findUserById($id));
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function edit($id)
        {
            //
            $this->layout->content = View::make('admin::pages.users.edit')
                                         ->with('user', Sentry::findUserById($id));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function update($id)
        {
            //
            dd(Input::all());
            Sentry::findUserById($id)
                  ->update(
                  array(
                      'first_name'      => Input::get('first_name'),
                      'last_name'       => Input::get('last_name'),
                      'email'           => Input::get('email'),
                      'avatar_provider' => Input::get('avatar_provider'),
                  )
                );
            Sentry::findUserById($id)
                  ->addGroup(Input::get('group'));

            $this->layout->content = View::make('admin::pages.users.edit')
                                         ->with('user', Sentry::findUserById($id));

        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function destroy($id)
        {
            $username = Sentry::findUserById($id)->username;
            Sentry::findUserById($id)
                  ->delete();
            return Redirect::route('admin.users.index')
                           ->with('message', "User <strong>{$username}</strong> Deleted!");
        }

        public function topReferrers()
        {
            $this->layout->content = View::make('admin::pages.users.referrers')
                                         ->with('users', User::topReferrers());
        }

    }