<?php

    namespace App\Modules\Admin\Controllers;

    use Redirect;
    use Input;
    use Response;
    use Str;
    use View;

    class UpdatesController extends \BaseController
    {

        protected $layout = 'admin::layout/master';


        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index()
        {
            //
            $this->layout->content = View::make('admin::pages.updates.index')
                                         ->with(
                                         'updates',
                                             \Updates::orderBy('created_at', 'desc')
                                                     ->get()
                );

        }

        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function create()
        {
            //
            $this->layout->content = View::make('admin::pages.updates.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @return Response
         */
        public function store()
        {
            //
            $update = new \Updates();
            $update->title = Input::get('title');
            $update->body = Input::get('body');
            $update->visible = Input::get('visible');
            $update->save();

            $this->layout->content = View::make('admin::pages.updates.edit')
                                         ->with('update', $update);
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function show($id)
        {
            //
            $this->layout->content = View::make('admin::pages.updates.edit')
                                         ->with('update', \Updates::find($id));
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function edit($id)
        {
            //
            $this->layout->content = View::make('admin::pages.updates.edit')
                                         ->with('update', \Updates::find($id));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function update($id)
        {
            //
            \Updates::find($id)->update(array(
                    'title' => Input::get('title'),
                    'body' => trim(Input::get('body')),
                    'visible' => Input::has('visible')
                ));

            $this->layout->content = View::make('admin::pages.updates.edit')
                                         ->with('update', \Updates::find($id));

        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return Response
         */
        public function destroy($id)
        {
            $title = \Updates::find($id)->title;
            \Updates::find($id)->delete();
            return Redirect::route('admin.updates.index')->with('message', "Update <strong>{$title}</strong> Deleted!");
        }

    }