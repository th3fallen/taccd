<?php

    Route::filter(
         'auth.admin',
             function () {
                 if (!Sentry::check()) {
                     return Redirect::guest('/');
                 }

                 if (!Sentry::getUser()
                           ->hasAccess('admin.dashboard')
                 ) {
                     return Redirect::guest('/');
                 }
             }
    );

Route::filter('userCan',
    function ($route, $request, $value) {
        if (!Sentry::getUser()
                  ->hasAccess($value)
        ) {
            return Redirect::back()->with('message', 'You dont have permissions to preform this action');
        }
    });