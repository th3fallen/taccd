<?php

    /**
     * @author Clark Tomlinson  <fallen013@gmail.com>
     * @since 1/29/14, 12:05 PM
     * @link http://www.clarkt.com
     * @copyright Clark Tomlinson © 2014
     *
     */
    class Updates extends Eloquent
    {

        /**
         * @var string
         */
        protected $table = 'updates';

        protected $fillable = array('title', 'body', 'visible');


    }