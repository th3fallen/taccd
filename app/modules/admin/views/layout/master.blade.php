@include('admin::layout.partials.header')

@include('admin::layout.partials.nav')

<div id="cl-wrapper" class="fixed-menu">
    @include('admin::layout.partials.sidebar')
    @yield('content')
</div>
@include('admin::layout.partials.footer')