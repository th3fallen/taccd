<div class="cl-sidebar" data-position="right" data-step="1"
     data-intro="<strong>Fixed Sidebar</strong> <br/> It adjust to your needs.">
    <div class="cl-toggle"><i class="fa fa-bars"></i></div>
    <div class="cl-navblock">
        <div class="menu-space">
            <div class="content">
                <div class="side-user">
                    <div class="avatar"><img src="{{Sentry::getUser()->avatar()}}" height="50" alt="Avatar"/></div>
                    <div class="info">
                        <a href="index.html#">{{Sentry::getUser()->fullName()}}</a>
                        <img src="{{image('state_online.png')}}" alt="Status"/> <span>Online</span>
                    </div>
                </div>
                <ul class="cl-vnavigation">
                    <li><a href="#"><i class="fa fa-file"></i><span>Updates</span></a>
                        <ul class="sub-menu">
                            {{HTML::clever_link('admin.updates.create', 'Add Update')}}
                            {{HTML::clever_link('admin.updates.index', 'List Updates')}}
                        </ul>
                    </li>
                    <li><a href="#"><i class="fa fa-users"></i><span>Users</span></a>
                        <ul class="sub-menu">
                            {{HTML::clever_link('admin.users.create', 'Add User')}}
                            {{HTML::clever_link('admin.users.index', 'List Users')}}
                            {{HTML::clever_link('admin.users.referrer', 'Top Referrers')}}
                        </ul>
                    </li>

                </ul>
            </div>
        </div>
        <div class="text-right collapse-button" style="padding:7px 9px;">
            <input type="text" class="form-control search" placeholder="Search..."/>
            <button id="sidebar-collapse" class="btn btn-default" style=""><i style="color:#fff;"
                                                                              class="fa fa-angle-left"></i></button>
        </div>
    </div>
</div>