
<script type="text/javascript" src="{{asset('assets/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/jquery.gritter/js/jquery.gritter.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/jquery.nanoscroller/jquery.nanoscroller.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/behaviour/general.js')}}"></script>
<script src="{{asset('assets/jquery.ui/jquery-ui.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/jquery.sparkline/jquery.sparkline.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/jquery.easypiechart/jquery.easy-pie-chart.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/jquery.nestable/jquery.nestable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/bootstrap.switch/bootstrap-switch.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('assets/jquery.select2/select2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/skycons/skycons.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/bootstrap.slider/js/bootstrap-slider.js')}}" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
@yield('footerjs')


<!-- Bootstrap core JavaScript
  ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript">
    $(document).ready(function(){
        //initialize the javascript
        App.init();
        @yield('jsfunctions')
    });
</script>
<script src="{{asset('assets/behaviour/voice-commands.js')}}"></script>
<script src="{{asset('assets/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/jquery.flot/jquery.flot.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/jquery.flot/jquery.flot.pie.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/jquery.flot/jquery.flot.resize.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/jquery.flot/jquery.flot.labels.js')}}"></script>
</body>
</html>
