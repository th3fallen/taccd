<!-- Fixed navbar -->
<div id="head-nav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="fa fa-gear"></span>
            </button>
            <a class="navbar-brand" href="index.html#"><span>Taccd CP <sup>
                        <small>Beta</small>
                    </sup></span></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="index.html#">Home</a></li>
                <li><a href="index.html#about">About</a></li>
                <li class="dropdown">
                    <a href="index.html#" class="dropdown-toggle" data-toggle="dropdown">Contact <b
                            class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="index.html#">Action</a></li>
                        <li><a href="index.html#">Another action</a></li>
                        <li><a href="index.html#">Something else here</a></li>
                        <li class="dropdown-submenu"><a href="index.html#">Sub menu</a>
                            <ul class="dropdown-menu">
                                <li><a href="index.html#">Action</a></li>
                                <li><a href="index.html#">Another action</a></li>
                                <li><a href="index.html#">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="index.html#" class="dropdown-toggle" data-toggle="dropdown">Large menu <b
                            class="caret"></b></a>
                    <ul class="dropdown-menu col-menu-2">
                        <li class="col-sm-6 no-padding">
                            <ul>
                                <li class="dropdown-header"><i class="fa fa-group"></i>Users</li>
                                <li><a href="index.html#">Action</a></li>
                                <li><a href="index.html#">Another action</a></li>
                                <li><a href="index.html#">Something else here</a></li>
                                <li class="dropdown-header"><i class="fa fa-gear"></i>Config</li>
                                <li><a href="index.html#">Action</a></li>
                                <li><a href="index.html#">Another action</a></li>
                                <li><a href="index.html#">Something else here</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-6 no-padding">
                            <ul>
                                <li class="dropdown-header"><i class="fa fa-legal"></i>Sales</li>
                                <li><a href="index.html#">New sale</a></li>
                                <li><a href="index.html#">Register a product</a></li>
                                <li><a href="index.html#">Register a client</a></li>
                                <li><a href="index.html#">Month sales</a></li>
                                <li><a href="index.html#">Delivered orders</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right user-nav">
                <li class="dropdown profile_menu">
                    <a href="index.html#" class="dropdown-toggle" data-toggle="dropdown"><img alt="Avatar"
                                                                                              src="{{Sentry::getUser()->avatar()}}" height="30"/>
                        <span>{{Sentry::getUser()->fullName()}}</span> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="index.html#">My Account</a></li>
                        <li><a href="{{URL::route('account.preferences')}}">Profile</a></li>
                        <li><a href="index.html#">Messages</a></li>
                        <li class="divider"></li>
                        <li><a href="index.html#">Sign Out</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right not-nav">
                <li class="button dropdown">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i
                            class=" fa fa-comments"></i></a>
                    <ul class="dropdown-menu messages">
                        <li>
                            <div class="nano nscroller">
                                <div class="content">
                                    <ul>
                                        <li>
                                            <a href="index.html#">
                                                <img src="{{image('avatar2.jpg')}}" alt="avatar"/><span
                                                    class="date pull-right">13 Sept.</span> <span
                                                    class="name">Daniel</span> I'm following you, and I want your money!
                                            </a>
                                        </li>
                                        <li>
                                            <a href="index.html#">
                                                <img src="{{image('avatar_50.jpg')}}" alt="avatar"/><span
                                                    class="date pull-right">20 Oct.</span><span class="name">Adam</span>
                                                is now following you
                                            </a>
                                        </li>
                                        <li>
                                            <a href="index.html#">
                                                <img src="{{image('avatar4_50.jpg')}}" alt="avatar"/><span
                                                    class="date pull-right">2 Nov.</span><span
                                                    class="name">Michael</span> is now following you
                                            </a>
                                        </li>
                                        <li>
                                            <a href="index.html#">
                                                <img src="{{image('avatar3_50.jpg')}}" alt="avatar"/><span
                                                    class="date pull-right">2 Nov.</span><span class="name">Lucy</span>
                                                is now following you
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <ul class="foot">
                                <li><a href="index.html#">View all messages </a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="button dropdown">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="fa fa-globe"></i><span class="bubble">2</span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="nano nscroller">
                                <div class="content">
                                    <ul>
                                        <li><a href="index.html#"><i class="fa fa-cloud-upload info"></i><b>Daniel</b>
                                                is now following you <span class="date">2 minutes ago.</span></a></li>
                                        <li><a href="index.html#"><i class="fa fa-male success"></i> <b>Michael</b> is
                                                now following you <span class="date">15 minutes ago.</span></a></li>
                                        <li><a href="index.html#"><i class="fa fa-bug warning"></i> <b>Mia</b> commented
                                                on post <span class="date">30 minutes ago.</span></a></li>
                                        <li><a href="index.html#"><i class="fa fa-credit-card danger"></i> <b>Andrew</b>
                                                killed someone <span class="date">1 hour ago.</span></a></li>
                                    </ul>
                                </div>
                            </div>
                            <ul class="foot">
                                <li><a href="index.html#">View all activity </a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="button"><a href="javascript:;" class="speech-button"><i class="fa fa-microphone"></i></a>
                </li>
            </ul>

        </div>
        <!--/.nav-collapse animate-collapse -->
    </div>
</div>