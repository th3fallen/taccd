@section('title', 'Admin CP')
@section('content')
<div class="container-fluid" id="pcont">
	<div class="cl-mcont">
		<div class="row dash-cols">

			<div class="col-sm-12 col-md-12">
				<div class="block-flat">
					<div class="header">
						<h3>Edit User</h3>
					</div>
					<div class="content">

						<form role="form" method="post"
						      action="{{URL::route('admin.users.update', array('id' => $user->id))}}">
							<div class="tab-container">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#account" data-toggle="tab">Account</a></li>
									<li class=""><a href="#profile" data-toggle="tab">Profile</a></li>
									<li><a href="#permissions" data-toggle="tab">Permissions</a></li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane cont active" id="account">
										<h3 class="hthin">Account Settings</h3>

										<div class="row">
											<div class="form-group col-md-6">
												<label>Username</label>
												<input type="text" name="username" readonly="readonly"
												       value="{{$user->username}}"
												       placeholder="Enter title"
												       class="form-control">
											</div>
											<div class="form-group col-md-6">
												<label>Email</label>
												<input type="text" name="email" value="{{$user->email}}"
												       placeholder="Enter title"
												       class="form-control">
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-6">
												<label>College</label>
												{{Form::select('college', College::lists('name', 'id'), $user->college,
												array('class' =>
												'select2'))}}
											</div>

											<div class="form-group">
												<label class="col-sm-3 control-label">Admin</label>

												<div class="col-sm-6">
													<div class="switch" data-on="success" data-on-text="Yes"
													     data-off-text="No">
														{{Form::checkbox('group', '1',
														$user->inGroup(Sentry::findGroupById(1)))}}
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane cont" id="profile">
										<h3 class="hthin">Profile Settings</h3>

										<div class="row">
											<div class="form-group col-md-6">
												<label>First Name</label>
												<input type="text" name="first_name" value="{{$user->first_name}}"
												       placeholder="Enter title"
												       class="form-control">
											</div>
											<div class="form-group col-md-6">
												<label>Last Name</label>
												<input type="text" name="last_name" value="{{$user->last_name}}"
												       placeholder="Enter title"
												       class="form-control">
											</div>
											<div class="form-group col-md-6">
												<label>Gravatar</label>

												<div class="switch" data-on="success" data-on-text="Yes"
												     data-off-text="No">
													{{Form::checkbox('avatar_provider', 'gravatar',
													($user->avatar_provider
													== 'gravatar'))}}
												</div>
											</div>
											@if($user->avatar_provider == 'custom')
											<div class="form-group">
												<label>Avatar</label>
												<input type="text" name="avatar" value="{{$user->avatar()}}"
												       placeholder="Enter title"
												       class="form-control">
											</div>
											@endif
										</div>
									</div>
									<div class="tab-pane" id="permissions">
										<h3 class="hthin">Permissions</h3>

										..sdfsdfsfsdf.
									</div>
								</div>
							</div>
							<input type="hidden" name="_method" value="PUT"/>
							<button class="btn btn-primary" type="submit">Submit</button>
							<button class="btn btn-default">Cancel</button>
						</form>

					</div>
				</div>
			</div>

		</div>
	</div>
	@stop