@section('title', 'Admin CP')
@section('jsfunctions')
App.dataTables({
'aaSorting': [[1, 'desc']]
});
@stop
@section('footerjs')
@if(Session::has('message'))
<script type="text/javascript">
    $.gritter.add({
        title: 'User Deleted!',
        text: "{{Session::get('message')}}",
        class_name: 'clean',
        time: ''
    });
</script>
@endif
<link rel="stylesheet" type="text/css" href="{{asset('assets/jquery.datatables/bootstrap-adapter/css/datatables.css')}}" />
<script type="text/javascript" src="{{asset('assets/jquery.datatables/jquery.datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/jquery.datatables/bootstrap-adapter/js/datatables.js')}}"></script>
@stop
@section('content')
<div class="container-fluid" id="pcont">
    <div class="cl-mcont">
        <div class="row dash-cols">

            <div class="col-sm-12 col-md-12">
                <div class="content">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="datatable">
                            <thead>
                            <tr>
                                <th>Username</th>
                                <th>Registered</th>
                                <th>Admin</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($users as $index => $user)
                            <tr class="gradeA @if($index % 2 == 0) even @else odd @endif" data-id="{{$user->id}}">
                                <td>{{$user->username}}</td>
                                <td>{{$user->created_at->timezone('America/New_York')->toDayDateTimeString()}}</td>
                                <td>{{Sentry::findUserById($user->id)->inGroup(Sentry::findGroupById(1))}}</td>
                                <td class="center">
                                    <div class="btn-group">
                                        <button class="btn btn-default btn-xs" type="button">Actions</button>
                                        <button data-toggle="dropdown" class="btn btn-xs btn-primary dropdown-toggle"
                                                type="button"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul role="menu" class="dropdown-menu pull-right">
                                            <li>{{HTML::linkRoute('admin.users.edit', 'Edit', array('id' =>
                                                $user->id))}}
                                            </li>
                                            <li>{{HTML::linkRoute('admin.users.show', 'Details', array('id' =>
                                                $user->id))}}
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                {{ Form::open(array('route' => array('admin.users.destroy',
                                                $user->id), 'method' => 'delete')) }}
                                                <a href="javascript:void(0);"
                                                   onclick="$(this).closest('form').submit();">Delete</a>
                                                {{ Form::close() }}
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @stop