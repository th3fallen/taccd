@section('title', 'Admin CP')
@section('jsfunctions')
App.dataTables({
'aaSorting': [[1, 'desc']]
});
@stop
@section('footerjs')
@if(Session::has('message'))
<script type="text/javascript">
    $.gritter.add({
        title: 'User Deleted!',
        text: "{{Session::get('message')}}",
        class_name: 'clean',
        time: ''
    });
</script>
@endif
<link rel="stylesheet" type="text/css"
      href="{{asset('assets/jquery.datatables/bootstrap-adapter/css/datatables.css')}}"/>
<script type="text/javascript" src="{{asset('assets/jquery.datatables/jquery.datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/jquery.datatables/bootstrap-adapter/js/datatables.js')}}"></script>
@stop
@section('content')
<div class="container-fluid" id="pcont">
    <div class="cl-mcont">
        <div class="row dash-cols">

            <div class="col-sm-12 col-md-12">
                <div class="content">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="datatable">
                            <thead>
                            <tr>
                                <th>Username</th>
                                <th>Referrals</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($users as $index => $user)
                            <tr class="gradeA @if($index % 2 == 0) even @else odd @endif" data-id="{{$user->id}}">
                                <td>{{$user->username}}</td>
                                <td>{{$user->refcount}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @stop