@section('title', 'Admin CP')
@section('jsfunctions')
App.textEditor();
@stop
@section('footerjs')
<script src="{{asset('assets/ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('assets/ckeditor/adapters/jquery.js')}}"></script>
@stop
@section('content')
<div class="container-fluid" id="pcont">
    <div class="cl-mcont">
        <div class="row dash-cols">

            <div class="col-sm-12 col-md-12">
                <div class="block-flat">
                    <div class="header">
                        <h3>Add Update</h3>
                    </div>
                    <div class="content">

                        <form role="form" method="post"
                              action="{{URL::route('admin.updates.update', array('id' => $update->id))}}">
                            <div class="form-group">
                                <label>Update Title</label>
                                <input type="text" name="title" value="{{$update->title}}" placeholder="Enter title"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Update Body</label>
                                <textarea class="ckeditor form-control" name="body" cols="30"
                                          rows="10">{{$update->body}}</textarea>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Publish</label>
                                <div class="col-sm-6">
                                    <div class="switch" data-on="success" data-on-text="Yes" data-off-text="No">
                                        {{Form::checkbox('visible', '1', $update->visible)}}
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="_method" value="PUT"/>
                            <button class="btn btn-primary" type="submit">Submit</button>
                            <button class="btn btn-default">Cancel</button>
                        </form>

                    </div>
                </div>
            </div>

        </div>
    </div>
    @stop