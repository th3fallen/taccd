<?php

    /*
    |--------------------------------------------------------------------------
    | Application Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register all of the routes for an application.
    | It's a breeze. Simply tell Laravel the URIs it should respond to
    | and give it the Closure to execute when that URI is requested.
    |
    */

    /**
     * Handle Routes for Admin CP
     */

    Route::group(
         array('prefix' => 'admin', 'before' => 'auth.admin'),
             function () {
                 Route::get(
                      '/',
                          array(
                              'as'   => 'admin.index',
                              'uses' => 'App\Modules\Admin\Controllers\AdminController@index'
                          )
                 );
                 Route::resource('updates', 'App\Modules\Admin\Controllers\UpdatesController');

                 Route::group(
                      array('before' => 'userCan:users.manage'),
                          function () {
                              Route::get('users/referrers', array('as' => 'admin.users.referrer', 'uses' => 'App\Modules\Admin\Controllers\UsersController@topReferrers'));
                              Route::resource('users', 'App\Modules\Admin\Controllers\UsersController');
                          }
                 );
             }
    );

// Add a micro for smart links
    HTML::macro(
        'clever_link',
            function ($route, $text, $attributes = array()) {
                $defaults = array('class' => '', 'title' => $text);
                $attributes = $attributes + $defaults;

                if (URL::route($route)) {
                    $href = URL::route($route);
                } else {
                    $href = URL::to($route);
                }
                $isActive = (Request::url() == $href);

                if ($isActive) {
                    $attributes['class'] .= ' active';
                }

                return '<li ' . HTML::attributes($attributes) . '>' . link_to($href, $text, $attributes) . '</li>';

            }
    );