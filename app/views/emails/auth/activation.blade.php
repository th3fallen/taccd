<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Account Activation</h2>

<div>
    Hello {{$username}},

    To activate your account, please visit this url: {{ URL::to('account/activate', array($activationCode)) }}.
</div>
</body>
</html>