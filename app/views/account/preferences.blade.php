@section('title', 'Preferences')
@section('content')
<div class="account-page">
    <div class="alert-box hide"></div>
    <div class="row">
        @include('account.forms.personalInformation')
        @include('account.forms.accountInformation')
    </div>
    <div class="row">
        @include('account.forms.changePassword')
        @include('account.forms.closeAccount')
    </div>

</div>
@stop
