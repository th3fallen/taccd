<div class="content content_element large-6 columns border_radius_5">
    <div class="content content_header">
        Deactivate Account
    </div>
    <div class="content content_text large-12 columns">
        {{Form::open(array('route' => 'account.postCloseAccount', 'data-abide', 'id' => 'closeAccount'))}}

        <div class="input-wrapper">
            <div class="large-4 columns">
                {{Form::label('oldpass', 'Current Password', array('class' => 'right inline'))}}
            </div>
            <div class="large-8 columns">
                {{Form::text('oldpass', false, array('class' => 'input'))}}
            </div>
        </div>

        <div class="large-6 large-centered columns">
        {{Form::submit('Close Account', array('class' => 'button radius other_submit'))}}
        </div>
        {{Form::close()}}
    </div>
</div>