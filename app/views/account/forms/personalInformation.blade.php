<div class="content content_element large-6 columns border_radius_5">
    <div class="content content_header">
        Personal Information

        <?php
            $user = Sentry::getUser();
            if ($user->college) {
                $college = $user->colleges->name;
            } else {
                $college = '';
            }
        ?>
    </div>
    <div class="content content_text large-12 columns">
        {{Form::open(array('route' => 'account.postPersonalInfo', 'data-abide'))}}
        <div class="input-wrapper row">
            <div class="large-3 columns">
                {{Form::label('firstname', 'First Name', array('class' => 'right inline'))}}
            </div>
            <div class="large-9 columns">
                {{Form::text('firstname', $user->first_name, array('class' => 'input'))}}
            </div>
        </div>

        <div class="input-wrapper row">
            <div class="large-3 columns">
                {{Form::label('lastname', 'Last Name', array('class' => 'right inline'))}}
            </div>
            <div class="large-9 columns">
                {{Form::text('lastname', $user->last_name, array('class' => 'input'))}}
            </div>
        </div>

        <div class="input-wrapper row">
            <div class="large-3 columns">
                {{Form::label('college', 'College', array('class' => 'right inline'))}}
            </div>
            <div class="large-9 columns">
                {{Form::text('college', $college, array('class' => 'input'))}}
            </div>
        </div>


        <div class="large-6 large-centered columns">
            {{Form::submit('Save Changes', array('class' => 'button radius other_submit'))}}
        </div>
        {{Form::close()}}
    </div>
</div>