<div class="content content_element large-6 columns border_radius_5">
    <div class="content content_header">
        Change Password
    </div>
    <div class="content content_text large-12 columns">
        {{Form::open(array('route' => 'account.postChangePassword', 'data-abide'))}}
        <div class="input-wrapper row">
            <div class="large-4 columns">
                {{Form::label('newpass', 'New Password', array('class' => 'right inline'))}}
            </div>
            <div class="large-8 columns">
                {{Form::password('newpass', array('id' => 'newpass', 'class' => 'input', 'required', 'pattern' => 'alpha_numeric'))}}
                <small class="error">Your new password is required</small>
            </div>
        </div>

        <div class="input-wrapper row">
            <div class="large-4 columns">
                {{Form::label('newpassconf', 'Confirm', array('class' => 'right inline'))}}
            </div>
            <div class="large-8 columns">
                {{Form::password('newpassconf', array('class' => 'input', 'required', 'pattern' => 'alpha_numeric', 'data-equalto' => 'newpass'))}}
                <small class="error">Your Passwords dont match</small>
            </div>
        </div>

        @if(empty(Sentry::getUser()->access_token))
        <div class="input-wrapper row">
            <div class="large-4 columns">
                {{Form::label('oldpass', 'Old Password', array('class' => 'right inline'))}}
            </div>
            <div class="large-8 columns">
                {{Form::password('oldpass', array('class' => 'input', 'required', 'pattern' => 'alpha_numeric'))}}
                <small class="error">Your old password is required</small>
            </div>
        </div>
        @endif

        <div class="large-6 large-centered columns">
        {{Form::submit('Change Password', array('class' => 'button radius other_submit'))}}
        </div>
        {{Form::close()}}
    </div>
</div>