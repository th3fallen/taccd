<div class="content content_element large-6 columns border_radius_5" id="accountInformation">
    <div class="content content_header">
        Acount Information

        <?php
            $user = Sentry::getUser();
            $gravatar = false;
            $custom = false;
            if ($user->avatar_provider == 'gravatar') {
                $gravatar = true;
            } else {
                $custom = true;
            }
        ?>
    </div>
    <div class="content content_text large-12 columns">
        {{Form::open(array('route' => 'account.postAccountInfo', 'files' => true, 'data-abide'))}}
        <div class="input-wrapper row">
            <div class="large-4 columns">
                {{Form::label('username', 'Username', array('class' => 'right inline'))}}
            </div>
            <div class="large-8 columns">
                {{Form::text('username', $user->username, array('class' => 'input'))}}
            </div>
        </div>

        <div class="input-wrapper row">
            <div class="large-4 columns">
                {{Form::label('email', 'E-mail Address', array('class' => 'right inline'))}}
            </div>
            <div class="large-8 columns">
                {{Form::text('email', $user->email, array('class' => 'input', 'required', 'pattern' => 'email'))}}
                <small class="error">You must enter a valid email</small>
            </div>
        </div>

        <div class="input-wrapper row">
            <div class="large-4 columns">
                {{Form::label('referral_code', 'Referral Url', array('class' => 'right inline'))}}
            </div>
            <div class="large-8 columns">
                {{Form::text('referral_code', 'http://taccd.com/?ref=' . $user->referral_code, array('readonly' => 'readonly'))}}
            </div>
        </div>

        <div class="input-wrapper row">
            <div class="large-4 columns">
                <label class="right">Avatar Provider</label>
            </div>
            <div class="large-8 columns">
                {{Form::checkbox('avatarProvider', 'gravatar', $gravatar, array('id' => 'gravatar'))}}
                {{Form::label('avatarProvider', 'Gravatar')}}
                {{Form::checkbox('avatarProvider', 'custom', $custom, array('id' => 'customAvatar'))}}
                {{Form::label('avatarProvider', 'Custom')}}
            </div>
        </div>

        <div class="input-wrapper row custom-avatar @if($user->avatar_provider !== 'custom')hide@endif">
            <div class="large-4 columns">
                {{Form::label('avatar', 'Avatar', array('class' => 'right inline'))}}
            </div>
            <div class="large-8 columns">
                {{Form::file('avatar', array('class' => 'input'))}}
            </div>
        </div>

        <div class="large-6 large-centered columns">
            {{Form::submit('Save Changes', array('class' => 'button radius other_submit'))}}
        </div>
        {{Form::close()}}
    </div>
</div>