@section('title', 'Taccd Updates')
@section('content')
<div class="row updates-page">
    <div class="large-12 columns content_element padded">
        <div class="content content_header">Taccd Updates</div>
        <ul class="unstyled">
            @foreach($updates as $update)
            <li>
                    <div class="row">
                        <div class="large-4 columns">
                            <h5>{{$update->title}}</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-4 columns">
                            <small>
                                {{$update->created_at->timezone('America/New_York')->toDayDateTimeString()}}
                            </small>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            {{$update->body}}
                        </div>
                    </div>
            </li>
            @endforeach
        </ul>

    </div>
</div>
@stop