@include('layout.partials.header')

@include('layout.partials.navigation')
@include('layout.partials.navigationUser')

@yield('content')

@include('layout.partials.footer')
