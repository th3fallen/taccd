@if(Sentry::check())
<div class="content_element row"
     style="margin-bottom: 15px; margin-top: 15px; margin-left: -11px; background-color: transparent; border: 0">
    <a class="submit other_submit border_radius_5 @if(User::likesCollege($college->id)) taccd @endif" id="taccd_school"
       data-id="{{$college->id}}">
        @if(User::likesCollege($college->id)) Remove Tacc @else Tacc School @endif
    </a>

    <div class="large-6 columns right">
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
        <div class="fb-share-button" data-href="{{URL::current()}}" data-type="button_count"></div>
        &nbsp;&nbsp;
        <span style="position: relative; top: 4px;"><a href="https://twitter.com/share" class="twitter-share-button"
                                                       data-lang="en" data-count="none">Tweet</a>
            <script>!function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (!d.getElementById(id)) {
                        js = d.createElement(s);
                        js.id = id;
                        js.src = "https://platform.twitter.com/widgets.js";
                        fjs.parentNode.insertBefore(js, fjs);
                    }
                }(document, "script", "twitter-wjs");</script>
        </span>
    </div>
</div>
@endif

<div class="content_element border_radius_5 content_hide_top row large-12 columns">

    <div class="content_header"> Top {{$college->abbreviation}} Confessions</div>

    <div class="content_text">

        @foreach($college->topConfessions as $topConfession)
        <div class="top_confession_element row">
            <span class="large-3 columns">

                @if($topConfession->anonymous)
                {{HTML::image('assets/' . $topConfession->college_id->avatar_url, false, array('class' => 'border_radius_3 top_confession_avatar')) }}
                 @else
            {{ HTML::image($topConfession->account_id->avatar(), false, array('class' => 'border_radius_3
                top_confession_avatar'))}}@endif

            </span>


            <span class="college_author"> {{$college->name}}</span>

            <div class="college_confession top_confession_limits large-9 columns">
                {{$topConfession->confession_text}}
            </div>

            <div class="college_confession_other large-12 columns push-3" style="clear:both;">

                <span id="top_confession_{{$topConfession->id}}">{{$topConfession->created_at->diffForHumans()}}</span>
                <span class="confession_bullet">&bull;</span>
                <span class="confession_red_text">{{$topConfession->likes->count()}} {{Str::plural('tacc', $topConfession->likes->count())}} </span>
                <span class="confession_bullet">&bull;</span>

                <span class="confession_red_text">{{$topConfession->comments->count()}}
                    {{Str::plural('comment', $topConfession->comments->count())}}
                </span>
            </div>
        </div>

        @endforeach

    </div>

</div>