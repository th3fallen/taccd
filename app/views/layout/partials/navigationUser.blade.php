<?php
    if (Sentry::check()) {
        $user = Sentry::getUser();
        ?>
        <div id="userbar_wrap">
            <div id="userbar" class="row">
                <div class="small-3 large-1 columns small-centered medium-uncentered large-uncentered">
                    <a href="{{URL::route('dashboard')}}" id="user_avatar" class="border_radius_5">
                        <div class="border_radius_3">
                            {{HTML::image($user->avatar())}}
                        </div>
                    </a>
                </div>

                <div class="small-8 large-3 columns small-centered small-only-text-center medium-uncentered large-uncentered" id="user_text_cell">
                    <span class="name">
                        @if(!empty($user->first_name))
                        {{HTML::linkRoute('dashboard', $user->fullName())}}
                        @else
                        {{HTML::linkRoute('account.preferences', 'Enter Your Name')}}
                        @endif
                    </span>

                    <span id="user_notify" class="user_notify border_radius_15">
                        216
                    </span>

                    <span class="college">
                        @if(!empty($user->college))
                        {{HTML::linkRoute('college', $user->colleges->name,
                        array(Str::slug($user->colleges->name)))}}
                        @else
                        {{HTML::linkRoute('account.preferences', 'Enter Your College')}}
                        @endif
                    </span>

                </div>
                <div id="userstats" class="small-5 small-centered small-only-text-center medium-uncentered large-uncentered large-5 columns">
                    <div class="user_stat">
                        <span class="user_text_top">
                            {{$user->confessions->count()}}</span> <br>
                        <span class="user_text_bottom">confessions</span>
                    </div>
                    <div class="user_stat">
                        <span class="user_text_top">
                            {{$user->likes->count()}}
                        </span> <br>
                        <span class="user_text_bottom">taccd posts</span>
                    </div>
                    <div class="user_stat">
                        <span class="user_text_top">
                            {{$user->collegeLikes->count()}}
                        </span> <br>
                        <span class="user_text_bottom">taccd schools</span>
                    </div>
                </div>

                <div class="pull-right">
                    <span id="user_settings_more" class="border_radius_3">
                        <i class="fa fa-pencil-square"></i>
                    </span>

                    <a href="#" data-dropdown="settingsDrop">
                        <span data-dropdown="settingsDrop" id="user_settings" class="border_radius_3">
                            <i data-dropdown="settingsDrop" class="fa fa-cogs"></i>
                            <i data-dropdown="settingsDrop" class="fa fa-caret-down"></i>
                        </span>
                    </a>

                    <div id="settingsDrop" data-dropdown-content class="f-dropdown">
                        <div class="drop_down_name">
                            @if(!empty($user->first_name))
                            {{$user->first_name}} {{$user->last_name}}
                            @else
                            {{HTML::linkRoute('account.preferences', 'Enter Your Name')}}
                            @endif
                        </div>
                        <div class="drop_down_college">
                            @if(!empty($user->college))
                            {{HTML::linkRoute('college', $user->colleges->name,
                            array(Str::slug($user->colleges->name)))}}
                            @else
                            {{HTML::linkRoute('account.preferences', 'Enter Your College')}}
                            @endif
                        </div>

                        @if($user->hasAccess('admincp'))
                        <div class="drop_down_link drop_down_admincp">
                            {{HTML::linkRoute('admin.index', 'Administration')}}
                        </div>
                        @endif

                        <div class="drop_down_link">
                            {{HTML::linkRoute('account.preferences', 'Edit Preferences')}}
                        </div>
                        <div class="drop_down_link">
                            @if(!empty($user->college))
                            {{HTML::linkRoute('college', 'My School',
                            array(Str::slug($user->colleges->name)))}}
                            @endif

                        </div>
                        <div class="drop_down_link">{{HTML::linkRoute('logout', 'Sign Out')}}</div>

                    </div>
                </div>
            </div>
        </div>
    <?php } ?>