<div class="footer_wrap full-row">
    <div class="footer-container row">
        <div class="footer large-10 columns">
            <span class="">&copy; 2013-{{ date('Y')}} <strong>Taccd.</strong></span>
            {{HTML::linkRoute('index', 'Home')}}
            <a href="#" class="search_request_form_link">School Request</a>
            <a href="javascript:void();">Advertise</a>
            <a href="#contact">Contact</a>
            {{HTML::linkRoute('terms', 'Terms')}}
            {{HTML::linkRoute('privacy', 'Privacy')}}
        </div>
    </div>
</div>
@include('layout.partials.modal')

<script type="text/javascript">
    Core.init();

    if (typeof Pages.<?php echo Route::currentRouteName(); ?> === 'function') {
        Pages.<?php echo Route::currentRouteName(); ?>();
    }
</script>
</body>
</html>