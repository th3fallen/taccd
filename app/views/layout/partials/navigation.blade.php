<div class="contain-to-grid">
    <nav class="top-bar" data-topbar>
        <ul class="title-area">
            <li class="name">
                <a href="{{URL::route('index')}}">
                    {{HTML::image('assets/taccd_logo.png')}}
                </a>
            </li>
            <li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
        </ul>

        <section class="top-bar-section">
            <!-- Right Nav Section -->
            <ul class="right">
                @if(!Sentry::check())
                <li><a href="#" class="register">Join</a></li>
                <li><a href="#" class="login">Log In</a></li>
                <li class="social"><a href="https://twitter.com/taccd"><i class="fa fa-twitter"></i></a></li>
                <li class="social"><a href="https://www.facebook.com/taccd"><i class="fa fa-facebook"></i></a></li>
                @endif

            </ul>

            <!-- Left Nav Section -->
            <ul class="left">
                <li>
                    <a href="{{URL::route('index')}}">
                        {{HTML::image('assets/home.png')}}
                        <span class="hide-for-medium-up">Home</span>
                    </a>
                </li>
                <li>
                    <a href="{{URL::route('discover')}}">
                        {{HTML::image('assets/discover.png')}}
                        <span class="hide-for-medium-up">Discover</span>
                    </a>
                </li>
                <li>
                    <a href="{{URL::route('index')}}">
                        {{HTML::image('assets/community.png')}}
                        <span class="hide-for-medium-up">Community</span>
                    </a>
                </li>
            </ul>
        </section>
    </nav>
</div>