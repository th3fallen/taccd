<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>Taccd - @yield('title')</title>

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<!--    Stylescripts-->
    {{ stylesheet_link_tag() }}


<!--    Scripts-->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-44189110-1', 'taccd.com');
        ga('send', 'pageview');

    </script>
    {{ javascript_include_tag() }}

    <script type="text/javascript">
        var baseUrl = '{{URL::route('index')}}/';
        var controllerUrl = '{{URL::current()}}/';
    </script>
    <link href="//fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="{{image('favicon.png')}}" type="image/x-icon"/>

    <meta name="description" content="The first social platform to share, discover, and discuss real college confessions from schools across the United States."/>

</head>
<body>