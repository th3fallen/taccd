<div class="college-page">
    <div class="content_element content_hide_top border_radius_5 row">

        <div class="content_header small-only-text-center medium-only-text-center"> Top Colleges</div>

        <div class="content_text">
            @foreach(College::topColleges(5) as $topCollege)

            <div class="top_confession_element row">
                <div
                    class="large-3 small-4 small-centered  small-only-text-center medium-only-text-center large-uncentered columns">
                    {{ HTML::image('assets/' . $topCollege->avatar_url, false, array('class' => 'border_radius_3
                    top_confession_avatar'))}}
                </div>


                <div
                    class="college_author large-9 small-10 small-centered  small-only-text-center medium-only-text-center large-uncentered columns"
                    style="padding-left: 0">
                    {{HTML::linkRoute('college', $topCollege->name,
                    array(Str::slug($topCollege->name)))}}
                </div>

                <br/>

                <div class="college_confession_other large-12 small-only-text-center medium-only-text-center">
                    <span class="confession_red_text">{{$topCollege->total_students}}</span> students
                    <span class="confession_bullet">•</span>
                    <span class="confession_red_text">{{$topCollege->total_confessions}}</span> confessions

                </div>
            </div>

            @endforeach

        </div>

    </div>

    <br/>

    <div class="content_element border_radius_5 row">

        <div class="content_header small-only-text-center medium-only-text-center"> Top Confessions</div>
        <div class="content_text">
            @foreach(Confession::topConfessions(5) as $topConfession)

            <div class="top_confession_element row">
                <div
                    class="large-3 small-4 small-centered  small-only-text-center medium-only-text-center large-uncentered columns">
                    {{ HTML::image('assets/' . $topConfession->college_id->avatar_url, false, array('class' =>
                    'border_radius_3
                    top_confession_avatar'))}}
                </div>


                <div
                    class="college_author large-9 small-6 small-centered  small-only-text-center medium-only-text-center large-uncentered columns"
                    style="padding-left: 0">
                    {{$topConfession->college_id->name}}
                </div>

                <div
                    class="college_confession large-9 small-12 small-centered  small-only-text-center medium-only-text-center large-uncentered columns"
                    style="padding-left: 0;">
                    {{$topConfession->confession_text}}
                </div>


                <div
                    class="college_confession_other large-7 small-8 columns left push-2  small-only-text-center medium-only-text-center"
                    style="margin-left: 3%">
                    <span class="confession_red_text">{{$topConfession->total_likes}}</span> likes
                    <span class="confession_bullet">•</span>
                    <span class="confession_red_text">{{$topConfession->total_comments}}</span> comments

                </div>
            </div>

            @endforeach

        </div>

    </div>

    <br/>

    <div class="content_element border_radius_5 row">

        <div class="content_header small-only-text-center medium-only-text-center"> Taccd Stats</div>

        <div class="content_text" id="stats">
            <div class="row">
                <div class="large-6 columns">
                    <div class="fa fa-users"></div>
                    <div class="stat_value">{{ $users }}</div>

                    <div class="stat_label">registered users</div>
                </div>
                <div class="large-6 columns">
                    <div class="fa fa-globe"></div>
                    <div class="stat_value">{{ $colleges }}</div>

                    <div class="stat_label">colleges listed</div>
                </div>
            </div>

            <div class="row">
                <div class="large-6 columns">
                    <div class="fa fa-comments-o"></div>
                    <div class="stat_value">{{ $confessions }}</div>

                    <div class="stat_label">posted confessions</div>
                </div>
                <div class="large-6 columns">
                    <div class="fa fa-heart"></div>
                    <div class="stat_value">{{ $likes }}</div>

                    <div class="stat_label">taccd confessions</div>
                </div>
            </div>
        </div>

    </div>
</div>