@include('layout.partials.header')

@include('layout.partials.navigation')
@include('layout.partials.navigationUser')

<div id="home_top">

    <span id="home_main_text">{{$college->name}}</span><br>

    <span id="college_sub_text">{{$college->city}}, {{Str::upper($college->state_code)}}</span> <br><br>


    <table cellpadding="0" cellspacing="0" border="0" style="margin:auto;">

        <tr>

            <td class="college_stat"> {{count($college->confessions)}} <br> <span
                    class="college_stat_name">confessions</span></td>

            <td class="college_stat_splitter"></td>

            <td class="college_stat"> {{count($college->likes)}} <br> <span
                    class="college_stat_name">student taccs</span></td>

    </table>


    <div class="submit_cover_art">
        <div class="link" onClick="javascript:load_form('school_art'); return false;">Looking to be featured on
            taccd? <strong>Submit your schools cover art!</strong></div>
    </div>

</div>
<div class="row">

    <div class="large-7 columns">
        @yield('content')
    </div>
    <div class="large-5 columns">
        @include('layout.partials.sidebar')
    </div>
</div>


@include('layout.partials.footer')
