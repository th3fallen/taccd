@include('layout.partials.header')

@include('layout.partials.navigation')
@include('layout.partials.navigationUser')
<div class="college-page">

    <div id="home_top" class="full-row large-12 columns"
         style="background-image: @if($college->background_url)url('{{image($college->background_url)}}') @elseurl('{{image('college_default_background_top.jpg')}}')@endif;">

        <span id="home_main_text" class="row large-12 columns">{{$college->name}}</span><br>

        <span id="college_sub_text" class="row large-12">{{$college->city}}, {{Str::upper($college->state_code)}}</span>
        <br><br>

        <div class="stats-container large-6 columns large-centered">
            <div class="college college_stat large-6 columns">
                <span class="confessionCount">
                    {{$college->confessionsCount->count()}}
                </span>
                <span class="college college_stat_name row">
                    Confessions
                </span>
            </div>

            <div class="college college_stat large-6 columns">
                <span class="taccCount">
                    {{$college->likes->count()}}
                </span>
                <span class="college college_stat_name row">
                    Student Taccs
                </span>
            </div>
        </div>


        <div class="submit_cover_art">
            <div class="link">
                Looking to be featured on taccd? <strong>Submit your schools cover art!</strong>
            </div>
        </div>

    </div>
    <div class="row">

        <div class="large-7 columns">
            @yield('content')
        </div>
        <div class="large-5 columns">
            @include('layout.partials.sidebar')
        </div>
    </div>
</div>


@include('layout.partials.footer')
