@include('layout.partials.header')

@include('layout.partials.navigation')
@include('layout.partials.navigationUser')


<div class="row">

    <div class="large-7 columns">
        @yield('content')
    </div>

    <div class="large-5 columns">
        @include('layout.partials.dashboardsidebar')
    </div>
</div>

@include('layout.partials.footer')
