@extends('layout.dashboard')
@section('title', 'Dashboard')
@section('content')
<div class="college-page dashboard-page">
    <div id="content_wrap">
        <div id="content_margin">
            <div class="panel callout radius">
                <a class="right panel-close">×</a>
                We ask that you please take a moment and spread the word about taccd to your friends. With your help, we
                can generate more confessions, stories, and laughs!
                <br/>
                <br/>

                <div class="row">
                    <div class="large-7 columns large-centered">

                        <div class="large-6 columns">
                            <div id="fb-root"></div>
                            <script>(function (d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id)) return;
                                    js = d.createElement(s);
                                    js.id = id;
                                    js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                    fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));</script>
                            <div class="fb-like" data-href="https://facebook.com/taccd" data-layout="button_count"
                                 data-action="like"
                                 data-show-faces="true" data-share="true"></div>
                        </div>

                        <div class="large-6 columns">
                            <script>!function (d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                                    if (!d.getElementById(id)) {
                                        js = d.createElement(s);
                                        js.id = id;
                                        js.src = p + '://platform.twitter.com/widgets.js';
                                        fjs.parentNode.insertBefore(js, fjs);
                                    }
                                }(document, 'script', 'twitter-wjs');</script>
                            <a href="https://twitter.com/share" class="twitter-share-button"
                               data-url="{{URL::route('index')}}"
                               data-text="Share, read, and discuss real colleges confessions from schools across the United States."
                               data-via="taccd" data-hashtags="taccd">Tweet</a>
                        </div>

                    </div>
                </div>
            </div>
            <div class="content_element border_radius_5_bottom small-12 columns">
                <div class="content_header"> Latest confessions</div>
                <div class="content_text row" id="college_confessions">
                    <div class="confession_wrapper">
                        @foreach (Confession::byLikedColleges(10) as $confession)
                        @include('loops.confessions')
                        @endforeach
                    </div>

                    @if(Confession::byLikedColleges(0)->count('id') > 10)
                    <div class="loadmoreConfessions loadmore" data-offset="10">
                        <div class="loadbg">
                            <span><a href="#">Load More Confessions</a></span>
                        </div>
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@stop