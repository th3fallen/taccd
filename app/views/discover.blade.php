@section('title', 'Discover')
@section('content')
<div class="discover-page">


    <div id="home_top" class="full-row" style="background-image: url('{{image('home_background_top.jpg')}}');">

        <span id="home_main_text">Discover Our Colleges.</span> <br>

        <div id="home_register" class="border_radius_5 large-6 columns large-centered" style="margin-bottom:37px;">

            <div id="home_register_inner" class="row">

                <span class="school_field large-4 columns">School Name</span>

                <div class="large-8 columns">
                    <div class="row collapse">
                        <div class="large-1 columns">
                            <span class="prefix"><i class="fa fa-search"></i></span>
                        </div>
                        <div class="large-11 columns">
                            <input type="text" id="college" placeholder="Begin your search..." autocomplete="off">
                        </div>
                    </div>
                </div>


            </div>

        </div>

    </div>


    <div id="content_wrap">

        <div id="content_margin" class="row">

            <div class="content_element content_hide_top border_radius_5_bottom small-12 columns centered">

                <a href="#" id="personal"></a>

                <div class="content_header">

                    Schools on Taccd

                    <div class="search_request_form_link">Don't see your college? Send in a <a href="#">request
                            form.</a>
                    </div>

                </div>
                <div class="row">
                    <div class="content_text centered small-12 columns" style="padding: 5px 30px 0 30px;">

                        <?php
                            $stateCount = 1;
                        ?>
                        @foreach ($states as $state)
                        @if($stateCount == 1)
                        <div class="row">
                            @endif
                            <div class="state_element small-4 columns">
                                <div class="state_header">
                                    {{ $state->name }}
                                </div>

                                <ul class="colleges">
                                    @foreach ($state->colleges as $college)
                                    <li class="college college_element">
                                        <a href="{{ URL::route('college', array('slug' => Str::slug($college->name))) }}">{{$college->name}}</a>
                                    </li>
                                    @endforeach
                                </ul>
                                @if(count($state->colleges) > 3)
                                <div class="discover_load_more" data-maxHeight="{{count($state->colleges) * 43 }}">
                                    <a href="#">Show More</a>
                                </div>
                                @endif

                            </div>
                            @if($stateCount == 3)
                        </div>
                        @endif
                        <?php
                            if ($stateCount == 3) {
                                $stateCount = 1;
                            } else {
                                $stateCount++;
                            }
                        ?>
                        @endforeach

                    </div>

                    <div style="clear:both;"></div>


                </div>
            </div>

        </div>

    </div>

</div>
@stop