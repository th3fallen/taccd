@section('title', $college->name)
@section('content')
<div id="content_wrap">
    <div id="content_margin">

        <div class="content_element content_hide_top border_radius_5_bottom small-12 columns">

            <div class="content_header"> Latest {{$college->abbreviation}} confessions</div>

            <div class="content_text row" id="college_confessions">

                <div class="confession_wrapper">
                    @foreach ($college->confessions as $confession)
                    @include('loops.confessions')
                    @endforeach

                </div>
                @if($college->confessions->count() > 9)
                <div class="loadmoreConfessions loadmore" data-offset="10">
                    <div class="loadbg">
                        <span><a href="#">Load More Confessions</a></span>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

@stop