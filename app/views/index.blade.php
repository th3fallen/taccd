@section('title', 'Uncover Real College Confessions.')
@section('content')

<div id="home_top" class="full-row" style="background-image: url('{{image('home_background_top.jpg')}}');">
    <span id="home_main_text">Uncover Real College Confessions.</span>
    <br>

    @include('auth.registerInline')

    <div class="row large-6 columns large-centered socal-wrapper">
        <div class="large-6 columns large-centered">

            <div class="large-4 columns">
                <div id="fb-root"></div>
                <script>(function (d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) return;
                        js = d.createElement(s);
                        js.id = id;
                        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>
                <div class="fb-like" data-href="https://facebook.com/taccd" data-layout="button_count"
                     data-action="like"
                     data-show-faces="true" data-share="true"></div>
            </div>

            <div class="large-4 columns">
                <script>!function (d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                        if (!d.getElementById(id)) {
                            js = d.createElement(s);
                            js.id = id;
                            js.src = p + '://platform.twitter.com/widgets.js';
                            fjs.parentNode.insertBefore(js, fjs);
                        }
                    }(document, 'script', 'twitter-wjs');</script>
                <a href="https://twitter.com/share" class="twitter-share-button" data-url="{{URL::route('index')}}"
                   data-text="Share, read, and discuss real colleges confessions from schools across the United States."
                   data-via="taccd" data-hashtags="taccd">Tweet</a>
            </div>

        </div>
    </div>
</div>

<div id="home_explore" class="full-row">
    <div id="home_explore_limiter" class="row">
        <div id="home_eplore_main_text" class="large-10 columns large-centered">Explore the Possibilites on Taccd.</div>
        <div class="row">

            <div class="explore_sub small-only-text-center medium-only-text-center large-4 columns">
                <div class="explore_sub_header">The Purpose of Taccd</div>
                <div class="explore_sub_content">Give students the opportunity to connect with others through
                    confessions.
                </div>
                <div class="explore_sub_link"><i class="fa fa-angle-right"></i>
                    <a href="#">Learn more about taccd</a>
                </div>
            </div>
            <div class="explore_sub small-only-text-center  medium-only-text-center large-4 columns">
                <div class="explore_sub_header">Share Your Confessions</div>
                <div class="explore_sub_content">Spill your secrets, share your epic tales and don't be afraid to tell
                    your most embarrassing moments.
                </div>
                <div class="explore_sub_link"><i class="fa fa-angle-right"></i>
                    <a href="#"
                       onclick="javascript:App.Modals.load('Register', baseUrl + 'account/register', 'tiny'); return false;">
                        Post
                        your first
                        confession</a>
                </div>
            </div>
            <div class="explore_sub small-only-text-center medium-only-text-center large-4 columns">
                <div class="explore_sub_header">Uncover College Secrets</div>
                <div class="explore_sub_content">Read what happens when the textbooks are shut and the confessions are
                    created.
                </div>
                <div class="explore_sub_link"><i class="fa fa-angle-right"></i>
                    <a href="#"
                       onclick="javascript:App.Modals.load('Register', baseUrl + 'account/register', 'tiny'); return false;">
                        Start reading confessions</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="explore_sub small-only-text-center medium-only-text-center large-4 columns">
                <div class="explore_sub_header">Tacc Your Favorite Schools</div>
                <div class="explore_sub_content">Stay updated with the recent gossip, rumors and stories from your
                    favorite schools.
                </div>
                <div class="explore_sub_link">
                    <i class="fa fa-angle-right"></i>
                    {{HTML::linkRoute('discover', 'Start searching for your schools')}}
                </div>
            </div>
            <div class="explore_sub small-only-text-center medium-only-text-center large-4 columns">
                <div class="explore_sub_header">Discover Campus Life</div>
                <div class="explore_sub_content">Compare colleges across the nation and read the latest stories from
                    their students.
                </div>
                <div class="explore_sub_link">
                    <i class="fa fa-angle-right"></i>
                    {{HTML::linkRoute('discover', 'Begin compairing colleges')}}
                </div>
            </div>
            <div class="explore_sub small-only-text-center medium-only-text-center large-4 columns">
                <div class="explore_sub_header">Interactive Forums</div>
                <div class="explore_sub_content">Interact with students through message boards where you can discuss a
                    variety of topics &amp; trends.
                </div>
                <div class="explore_sub_link"><i class="fa fa-angle-right"></i>
                    <a href="#">Join the conversation</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="full-row">
    <div id="taccd_stats">
        <div class="row large-8 columns large-centered">
            <div class="large-4 columns">
                <h2><?php echo $users ?></h2>

                <h2>registered users</h2>
            </div>
            <div class="large-4 columns" style="border-right: 1px solid #A63429;border-left: 1px solid #A63429;">
                <h2><?php echo $colleges ?></h2>

                <h2>colleges listed</h2>
            </div>

            <div class="large-4 columns">
                <h2><?php echo $confessions ?></h2>

                <h2>posted confessions</h2>
            </div>
        </div>

        <div class="row large-8 columns large-centered">
            <div class="large-6 columns">

                <h2><?php echo $likes ?></h2>

                <h2>taccd confessions</h2>
            </div>

            <div class="large-6 columns" style="border-left: 1px solid #A63429;">
                <h2><?php echo $comments ?></h2>

                <h2>posted comments</h2>
            </div>
        </div>
    </div>
</div>

@stop