    <div class="comment_element row">
        <span class="college_author_avatar large-2 columns">
            {{HTML::image($comment->account_id->avatar())}}
        </span>
        <span class="college_author large-10 columns">
            <span class="college_author_grey">{{$comment->account_id->username}} @ {{$confession->college_id->name}}</span>
        </span>

        <div class="college_confession comment_limit large-10 columns">
            {{{$comment->comment_text}}}
            <br/>
             <span class="college_confession_other large-12 columns row end ">
            {{$comment->created_at->diffForHumans()}}
        </span>
        </div>
    </div>