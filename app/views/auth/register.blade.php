{{Form::open(array('route' => 'register.post', 'data-abide', 'class' => 'registerForm'))}}
<div class="input-wrapper">
    {{Form::label('username', 'Username')}}
    {{Form::text('username', Input::old('username'), array('class' => 'input', 'required'))}}
    <small class="error">Username is Required</small>
</div>

<div class="input-wrapper">
    {{Form::label('email', 'Email')}}
    {{Form::text('email', Input::old('email'), array('class' => 'input', 'required', 'pattern' => 'email'))}}
    <small class="error">Email is Required</small>
</div>

<div class="input-wrapper">
    {{Form::label('password', 'Password')}}
    {{Form::password('password', array('class' => 'input', 'required', 'pattern' =>
    'alpha_numeric'))}}
    <small class="error">Password is Required</small>
</div>

<div class="small" style="font-size: 13px;">
    By clicking Join Now, you agree to our {{HTML::linkRoute('terms', 'Terms', array('target' => '_blank'))}} &amp;
    {{HTML::linkRoute('privacy', 'Privacy Policy', array('target' => '_blank'))}}.
</div>
<br/>
<a class="btn btn-block btn-social btn-facebook" href="{{URL::route('login.facebook')}}">
    <i class="fa fa-facebook"></i> Sign up with Facebook
</a>
<br/>
@if (Input::has('ref'))
{{Form::hidden('referral_code', Input::get('ref'))}}
@endif
{{Form::submit('Join Now', array('class' => 'button radius other_submit'))}}
{{Form::close()}}