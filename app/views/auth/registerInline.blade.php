{{Form::open(array('route' => 'register.post', 'data-abide', 'class' => 'registerForm'))}}
<div id="home_register" class="border_radius_5">


    <div id="home_register_inner" class="row">
        <div class="alert-box hide radius"></div>
        <div class="input-wrapper small-4 columns">
            {{Form::text('username', Input::old('username'), array('class' => 'input', 'placeholder' => 'Username',
            'required'))}}
            <small class="error">Username is Required</small>

        </div>

        <div class="input-wrapper small-4 columns">
            {{Form::text('email', Input::old('email'), array('class' => 'input', 'placeholder' => 'Email',
            'required'))}}
            <small class="error">Email is Required</small>

        </div>

        <div class="input-wrapper small-4 columns">
            {{Form::password('password', array('class' => 'input', 'placeholder' => 'Password', 'required', 'pattern' =>
            'alpha_numeric'))}}
            <small class="error">Password is Required</small>
        </div>
    </div>
</div>

<span id="form_text_error" class="form_text_error"></span>

<br><br>
<strong>By clicking Join Now, you agree to our {{HTML::linkRoute('terms', 'Terms', array(), array('target' =>
    '_blank'))}} &amp;
    {{HTML::linkRoute('privacy', 'Privacy Policy', array(), array('target' => '_blank'))}}.</strong>
@if (Input::has('ref'))
{{Form::hidden('referral_code', Input::get('ref'))}}
@endif
<div class="form_submit">
    {{Form::submit('Join Now', array('class' => 'button radius other_submit'))}}

</div>
{{Form::close()}}
