{{Form::open(array('route' => 'forgotPassword.post', 'data-abide', 'class' => 'forgotPasswordForm'))}}
<div class="small" style="font-size: 13px; margin-bottom: 15px;">
    Please enter your username in the box below to reset your password.
</div>
<div class="input-wrapper">
    {{Form::label('username', 'Username')}}
    {{Form::text('username', Input::old('username'), array('class' => 'input', 'required'))}}
</div>

<br/>

{{Form::submit('Reset Password', array('class' => 'button radius other_submit'))}}
{{Form::close()}}