    {{Form::open(array('route' => 'login.post', 'data-abide', 'class' => 'loginForm'))}}
    <div class="input-wrapper">
        {{Form::label('username', 'Username')}}
        {{Form::text('username', Input::old('username'), array('class' => 'input', 'required'))}}
        <small class="error">You must enter a username.</small>
    </div>

    <div class="input-wrapper">
        {{Form::label('password', 'Password')}}
        {{Form::password('password', array('class' => 'input', 'required', 'pattern' => 'alpha_numeric'))}}
        <small class="error">You must enter a password.</small>
    </div>

    <div class="field row">
        <label class="checkbox left" for="remember_me">
            {{Form::checkbox('remember_me', '1')}}
            Remember Me
        </label>
        <label class="right" for="">{{HTML::linkRoute('forgotPassword', 'Forgot Password?', array(), array('class' => 'forgotPassword'))}}</label>
    </div>

    <a class="btn btn-block btn-social btn-facebook" href="{{URL::route('login.facebook')}}">
        <i class="fa fa-facebook"></i> Sign in with Facebook
    </a>
    <br/>

    {{Form::submit('Login', array('class' => 'button radius other_submit'))}}
    {{Form::close()}}