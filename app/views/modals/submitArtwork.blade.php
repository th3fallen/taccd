{{Form::open(array('route' => 'submitArtwork.post', 'data-abide', 'class' => 'submitArtwork'))}}

<div class="input-wrapper">
    {{Form::label('schoolArt', 'School Art (link)')}}
    {{Form::text('schoolArt', false, array('class' => 'input', 'required'))}}
    <small class="error">This field is required</small>
</div>
{{Form::hidden('school_slug', $slug)}}
{{Form::submit('Submit Artwork', array('class' => 'button radius other_submit'))}}
{{Form::close()}}