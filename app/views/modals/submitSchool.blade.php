{{Form::open(array('route' => 'submitSchool.post', 'data-abide', 'class' => 'submitSchool'))}}

<div class="input-wrapper">
    {{Form::label('schoolName', 'School Name')}}
    {{Form::text('schoolName', false, array('class' => 'input', 'required'))}}
    <small class="error">This field is required</small>
</div>

<div class="input-wrapper">
    {{Form::label('schoolWebsite', 'School Website')}}
    {{Form::text('schoolWebsite', false, array('class' => 'input', 'required'))}}
    <small class="error">This field is required</small>
</div>
{{Form::submit('Submit School', array('class' => 'button radius other_submit'))}}
{{Form::close()}}