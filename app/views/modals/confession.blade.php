{{Form::open(array('route' => 'confession.post', 'data-abide', 'class' => 'confessionForm'))}}
<div class="input-wrapper">
    <small class="error">You cant confess nothing silly.</small>
    {{Form::textarea('confession', null, array('rows' => '5', 'style' => 'height: 106px;', 'required'))}}
</div>
<br/>
<div class="input-wrapper">
    {{Form::checkbox('anonymous', 1, 'true')}}
    {{Form::label('anonymous', 'Post Anonymously')}}
</div>
<br/>
{{Form::submit('Submit Confession', array('class' => 'button radius other_submit'))}}
{{Form::close()}}