<div class="college_confession_element row" data-url="{{URL::route('college', Str::slug($confession->college_id->name))}}">
    <div class="college_image_icons small-4 small-centered  small-only-text-center medium-only-text-center large-uncentered large-2 columns">
        <div class="college_avatar border">
            @if($confession->anonymous){{ HTML::image('assets/' . $confession->college_id->avatar_url) }} @else
            {{HTML::image($confession->account_id->avatar())}}@endif
        </div>
        <div class="college_icons">
            <div class="college_tacc_confession @if(User::likesConfession($confession->id))active@endif"
                 data-id="{{$confession->id}}">
                <i class="fa fa-heart"></i>
            </div>
            <div class="college_expand_confession">
                <i class="fa fa-comments"></i>
            </div>
            <div class="college_goto_comments">
                <i class="fa fa-mail-forward"></i>
            </div>

        </div>
    </div>

    <div class="college_confession_text small-12 small-centered  small-only-text-center medium-only-text-center large-uncentered large-10 columns">
        <div class="college_author">
            <span class="college_author_grey"> @if($confession->anonymous)Anonymous @else {{$confession->account_id->username}}@endif @</span>
            {{HTML::linkRoute('college', $confession->college_id->name,
            array(Str::slug($confession->college_id->name)))}}
        </div>
        <div class="college_confession">
            {{{$confession->confession_text}}}
        </div>
        <div class="college_confession_other">
            <span id="confession_time">{{$confession->created_at->diffForHumans()}}</span>
            <span class="confession_bullet">&bull;</span>
            <span class="expand"><a href="#">Expand</a></span>
            <span class="confession_bullet">&bull;</span>
            <span class="confession_red_text">
                <span class="likecount">{{$confession->likes->count()}}</span> {{Str::plural('tacc', $confession->likes->count())}}
            </span>
            <span class="confession_bullet">&bull;</span>

            <span class="confession_red_text">
                {{$confession->comments->count()}}
                {{Str::plural('comment', $confession->comments->count())}}
            </span>
        </div>
        <div class="confession_comment_container">
            <div class="confession_comments_element">
                <div class="confession_likes">
                    @foreach($confession->likes()->limit(9)->get() as $like)
                    {{HTML::image($like->account_id->avatar(), false, array('class' => 'like_avatar border_radius_5',
                    'title' =>
                    $like->account_id->username . ' has taccd this confession.'))}}
                    @endforeach
                </div>
            </div>

            @unless(Sentry::check())
            <p>Please Login to Comment</p>
            @endunless

            @if(Sentry::check())
            <div class="commentFormWrapper">
                {{Form::open(array('route' => 'comment.post', 'data-abide', 'class' => 'commentForm'))}}
                <div class="input-wrapper">
                    {{Form::textarea('confession', null, array('rows' => '5', 'style' => 'height: 68px;',
                    'required'))}}
                    <div class="alert-box hide"></div>
                </div>
                {{Form::hidden('confessionID', $confession->id)}}
                {{Form::close()}}
            </div>
            @endif

            @if (count($confession->comments))
            <div class="confession_comment_wrapper">
                @foreach ($confession->comments()->limit(2)->orderBy('created_at', 'desc')->get() as $comment)
                @include('partials.ajaxComments')
                @endforeach
            </div>

            @if(count($confession->comments) > 2)
            <div class="loadmoreComments loadmore" data-offset="2" data-confessionid="{{$confession->id}}">
                <div class="loadbg">
                    <span><a href="#">Load More Comments</a></span>
                </div>
            </div>
            @endif
            @endif
        </div>
    </div>
</div>
<div class="top_confession_splitter"></div>