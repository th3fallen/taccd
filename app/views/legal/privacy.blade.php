@extends('layout.fullwidth')
@section('title', 'Privacy Policy')

@section('content')

<div class="row">
    <div class="large-12 columns content_element padded">

        <h1>Privacy Policy</h1>

        <div class="terms_text">
            <p>Taccd takes your privacy seriously. This privacy policy describes what personal information we collect
                and
                how we use it. Taccd reserves the right to edit, add, and/or update this privacy policy at any time.<br>
                Last Revision:
                <br/>
                <br/>
                <strong>January 15th, 2014</strong>
                <br/>
            </p>
            <h4>Collection of Information</h4>

            <p>You may browse the Taccd website without registering or sending us any personal information. When you,
                (the user) registers on our website, Taccd saves and stores certain information about you upon
                completing the registration.</p>

            <p>Upon a successful registration, we will save, not limited to, the following information:
            </p>
            <ul>
                <li>Username</li>
                <li>Email</li>
                <li>Password</li>
                <li>IP Address</li>
            </ul>
            <p></p>

            <p>Before successfully posting a confession, additional information is required, which includes the
                following:
            </p>
            <ul>
                <li>First Name</li>
                <li>Last Name</li>
                <li>College</li>
            </ul>
            <p></p>

            <p>The following information may also be saved depending on the users activity:

            </p>
            <ul>
                <li>User avatars used to enhance the creativity of a users profile.</li>
                <li>Colleges a user has taccd in order to display confessions on the users dashboard.</li>
                <li>User taccs and comments on posted confessions.</li>
                <li>The total number of confessions, taccd posts and taccd schools a user has accumulated.</li>
            </ul>
            <p></p>

            <h4>Posting Confessions</h4>

            <p>All confessions on the Taccd website are strictly anonymous. No personal information is posted
                publicly.</p>

            <h4>Posting Comments</h4>

            <p>Posting comments on confessions allows other users to view your username on the Taccd website. Personal
                information such as, your name and email are not publicly viewable through comments.</p>

            <h4>Taccing Confessions</h4>

            <p>Taccing confessions allows other users to view your username by hovering your avatar when a confession is
                expanded.</p>

            <h4>Use of Information</h4>

            <p>User information is collected to help create a unique and protected environment for Taccd users.
                You agree that Taccd may use your information, in the ways listed below:
            </p>
            <ul>
                <li>Maintain the service you request.</li>
                <li>Help resolve support issues and resolve any user related problems.</li>
                <li>Help detect and anticipate any potential threats, malicious, or illegal activities.</li>
                <li>Change our advertising techniques, or advertising format.</li>
                <li>Contact you about our services, to further improve marketing, website expansions/updates, and Taccd
                    promotional offers.
                </li>
                <li>Discuss and compare user information for expansion and future marketing ideas.</li>
                <li>Information will never be sold or otherwise transferred to unaffiliated third parties without the
                    approval of the user at the time of collection.
                </li>
            </ul>
            <p></p>

            <h4>Disclosure of Your Information</h4>

            <p>Taccd may disclose your personal information in the following instances:
            </p>
            <ul>
                <li>The enforcing of our Terms of Service.</li>
                <li>Legal engagements and actions.</li>
                <li>Interacting with support claims that openly violate our terms or rights of other users.</li>
            </ul>
            <p></p>

            <h4>Changing of Profile Information</h4>

            <p>You may change your profile information by logging in and entering your "edit preferences" page.</p>

            <p>At any given time, you may edit your:
            </p>
            <ul>
                <li>Username</li>
                <li>Email Address</li>
                <li>Current Password</li>
                <li>Avatar</li>
            </ul>
            <p></p>

            <p>To change your current college, we ask that you please contact our support team.</p>

            <p>You may also deactivate your Taccd account if you're unsatisfied with our website for any given
                reason.</p>

            <h4>Cookies</h4>

            <p>Where necessary, Taccd uses cookies to store information about a visitor's preferences and history in
                order to better serve the visitor and/or present the visitor with customized content.</p>

            <p>Advertising partners and other third parties may also use cookies, scripts and/or web beacons to track
                visitors to our site in order to display advertisements and other useful information. Such tracking is
                done directly by the third parties through their own servers and is subject to their own privacy
                policies.</p>

            <p>Taccd also ultizes cookies with our remember me feature. This specific feature allows the user to remain
                logged in without entering your username/password credentials.</p>

            <h4>Account Security and Protection</h4>

            <p>Your password in terms of security is everything. To ensure your security on our website, we ask that you
                use creative passwords. Use numbers and special characters to help strengthen your password.</p>

            <p>We strongly suggest that you DO NOT hand out your Taccd password at any time. If you decide to share your
                password, keep in mind that you are responsible for all actions made by your account. If your account
                for some reason gets taken over, contact the Taccd support team immediately.</p>

            <p>Ensuring the security of our users and keeping the service online is our top priority at Taccd. To help
                ensure this; we use the latest technology to monitor networking traffic and unauthorized attempts of
                harm.</p>

            <p>We secure your information using very thorough procedures, policies, firewalls and Secure Socket Layers
                (SSL) encrypted connections. However, as a perfect security system does not exist, Taccd is not
                responsible for any kind of illegal interception or violation of its systems or databases by
                non-authorized parties and Taccd does not make any responsibility in the use of the information obtained
                by those mentioned cases.</p>

            <h4>Routine Information Collection</h4>

            <p>All web servers track basic information about their visitors. This information includes, but is not
                limited to, IP addresses, browser details, timestamps and referring pages. None of this information can
                personally identify specific visitors to this site. The information is tracked for routine
                administration and maintenance purposes.</p>

            <h4>Controlling Your Privacy</h4>

            <p>Note that you can change your browser settings to disable cookies if you have privacy concerns. Disabling
                cookies for all sites is not recommended as it may interfere with your use of some sites. The best
                option is to disable or enable cookies on a per-site basis. Consult your browser documentation for
                instructions on how to block cookies and other tracking mechanisms.</p>

            <h4>Special Note About Google Advertising</h4>

            <p>Any advertisements served by Google, Inc., and affiliated companies may be controlled using cookies.
                These cookies allow Google to display ads based on your visits to this site and other sites that use
                Google advertising services. Learn how to opt out of Google's cookie usage. As mentioned above, any
                tracking done by Google through cookies and other mechanisms is subject to Google's own privacy
                policies.</p>

            <h4>Consent</h4>

            <p>By using the Taccd website, you have given us your consent to the use and collection of your information
                explained in this Privacy Policy. This policy may change time from time, and we may do so without the
                notice of the user. All changes will be posted on this page. You confirm and agree to such changes, and
                we ask you regularly check this page for updates.</p>

            <p>Taccd welcomes all concerns, questions or comments about our Privacy Policy. Feel free to open a support
                ticket about this policy at any time!</p>

        </div>
    </div>
</div>

@stop