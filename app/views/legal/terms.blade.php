@extends('layout.fullwidth')
@section('title', 'Terms of Service')

@section('content')

<div class="row">
    <div class="large-12 columns content_element padded">

        <h1>Terms of Service</h1>

        <div class="terms_text">

            <p>
                Taccd reserves the right to edit, add, and/or update these terms and conditions at any time.<br>
                Last Revision:
                <br/>
                <br/>
                <strong>January 15th, 2014</strong>
                <br/>
            </p>

            <h4>Acceptance of Terms and Services</h4>

            <p>By registering on our website, you comply and agree to our following terms which may be updated from time
                to time
                without notice to the user. You may always find the latest version of the Terms of Service on this
                page.</p>

            <h4>Privacy Policy</h4>

            <p>By using our service and website, you comply and agree to the terms of our Privacy Policy which may be
                updated
                from time to time without notice to the user. </p>

            <h4>User Registration</h4>

            <p>Before accessing the full features of Taccd, you will need to register for a free account. Before
                completing our
                registration, you understand and comply to our Terms of Service and Privacy Policy.
            </p>

            <p>This includes:</p>
            <ol>
                <li>Complying with our rules.</li>
                <li>Providing true and accurate information.</li>
                <li>Maintaining and updating your personal information as needed. Users found to be providing false,
                    inaccurate
                    information, or violating the rules may have their account suspended or permanently terminated
                    without their
                    consent.
                </li>
            </ol>
            <p></p>

            <p>Taccd, under its sole discretion, may change, edit, modify, or add features to the website. All changes
                and the
                effects of changes may be updated without notice. However, most substantial updates will be posted
                through the
                use of our social media pages. Any changes to the Privacy Policy or Terms of Service will be effective
                on the
                posting date. Users registered prior to the changes accept any alterations, and confirm your agreement
                to the
                announced modifications.</p>

            <h4>While Using Taccd</h4>

            <p>During the use of our site, you will NOT:
            </p>
            <ul>
                <li>Violate the Terms of Service, Privacy Policy, and/or United States laws.</li>
                <li>Use our services if temporarily or indefinitely suspended from using our site or services.</li>
                <li>Post malicious content.</li>
                <li>Display or post spam throughout our website.</li>
                <li>Upload inappropriate user avatars.</li>
                <li>Distribute viruses or malicious content that may harm the Taccd website or its users.</li>
            </ul>
            <p></p>

            <h4>You Post It, You Own It</h4>

            <p> We don't own anything you decide to post on the Taccd website. All comments are published under the
                Creative
                Commons Attribution - No Derivative Works 3.0 license, with the attribution being to you, the author.
                Confessions posted anonymously is published as public domain.</p>

            <h4>What We Don't Want to See</h4>

            <p> We love seeing your confessions, but we will remove anything we consider: offensive, sexist, prejudiced,
                racist,
                threatening, or involves any sort of spam. Users caught violating the rules may have their account
                suspended or
                permanently terminated without their consent.</p>

            <h4>User Avatars</h4>

            <p>On the Taccd website you may upload and display user avatars. By uploading your content you understand
                and
                confirm that:
            </p>
            <ol>
                <li>You own or have permission from the content owner.</li>
                <li>Any content uploaded is visible publicly on the site and may be seen elsewhere.</li>
            </ol>
            <p></p>

            <p>Taccd does not review all user profile pictures submitted and uploaded. However, we may remove any
                content, with
                or without your permission. The following categories but not limited to, deem inappropriate.
            </p>
            <ol>
                <li>Malicious.</li>
                <li>Abusive.</li>
                <li>Obscene.</li>
                <li>Violates our terms or privacy policy.</li>
                <li>Violates users rights or personal privacy.</li>
            </ol>

            <h4>User Abuse</h4>

            <p>Taccd strives to continuously keep our community protected and our service working properly. The staff at
                Taccd
                asks that you please report any potential threats or violations to our terms. You may notify us, by
                submitting a
                support ticket.</p>

            <p>By agreeing to our Terms of Service, you understand that we may immediately suspend or terminate your
                right to
                use the Taccd service. Upon the termination of a users account, we may enduringly delete all of your
                content/information and any content that may associate back to you. Users caught violating or trying to
                evade
                the suspended time period may result in a permanent suspension.</p>

            <h4>COPPA</h4>

            <p>The content of this website is not intended towards persons under the age of the 13. Taccd does not
                knowingly
                collect, store, or transmit the personal information of anyone under the age of 13. If Taccd is informed
                that
                any such user is viewing or using the content of this webiste, we will take appropriate actions to
                cancel that
                user's account and block their access.</p>


            <h4>Consent</h4>

            <p>By registering an account on the Taccd website, you have confirmed your agreement to these Terms of
                Service. This
                policy may change time from time, and we may do so without notice to the user. All changes will be
                posted on
                this page. You confirm and agree to such changes, and we ask you regularly check this page for
                updates.</p>

            <p>Taccd welcomes all concerns, questions or comments about our Terms of Service. Feel free to contact us
                about this
                policy at any time!</p>

        </div>
    </div>
</div>
<br/>

@stop