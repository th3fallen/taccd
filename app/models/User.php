<?php

    /**
     * @author Clark Tomlinson  <fallen013@gmail.com>
     * @since 2/3/14, 7:43 PM
     * @link http://www.clarkt.com
     * @copyright Clark Tomlinson © 2014
     *
     */
    class User extends \Cartalyst\Sentry\Users\Eloquent\User
    {


        /**
         * @return \Illuminate\Database\Eloquent\Relations\HasOne
         */
        public function colleges()
        {
            return $this->hasOne('College', 'id', 'college');
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function confessions()
        {
            return $this->hasMany('Confession', 'account_id', 'id');
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function likes()
        {
            return $this->hasMany('ConfessionLikes', 'account_id', 'id');
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function collegeLikes()
        {
            return $this->hasMany('CollegeLikes', 'account_id', 'id');
        }

        /**
         * @param null $size
         *
         * @return mixed|string|void
         */
        public function avatar($size = null)
        {
            if ($this->avatar_provider == 'custom') {
                if (filter_var($this->avatar_url, FILTER_VALIDATE_URL)) {
                    return $this->avatar_url;
                }
                return image($this->avatar_url);
            }
            return Gravatar::src($this->email, $size);
        }

        /**
         * @param $value
         *
         * @return string
         */
        public function getAvatarUrlAttribute($value)
        {
            if (empty($value)) {
                return 'avatars/default2.png';
            }
            return $value;
        }

        /**
         * @return string
         */
        public function fullName()
        {
            return $this->first_name . ' ' . $this->last_name;
        }

        /**
         * @param $value
         *
         * @return string
         */
        public function getFirstNameAttribute($value)
        {
            return ucwords($value);
        }

        /**
         * @param $value
         *
         * @return string
         */
        public function getLastNameAttribute($value)
        {
            return ucwords($value);
        }

        /**
         * @param $collegeId
         *
         * @return int
         */
        public static function likesCollege($collegeId)
        {
            if (Sentry::check()) {
                $user = Sentry::getUser();

                return CollegeLikes::where('college_id', '=', $collegeId)
                                   ->where('account_id', '=', $user->id)
                                   ->count();
            }
            return false;
        }

        /**
         * @param $confessionID
         *
         * @return bool|int
         */
        public static function likesConfession($confessionID)
        {
            if (Sentry::check()) {
                $user = Sentry::getUser();

                return ConfessionLikes::where('confession_id', '=', $confessionID)
                                      ->where('account_id', '=', $user->id)
                                      ->count();
            }
            return false;
        }

        /**
         * @param $refCode
         *
         * @return bool
         */
        public static function findByReferralCode($refCode)
        {

            $user = User::where('referral_code', '=', $refCode);

            if ($user->count()) {
                return $user->first()->id;
            }
            return null;
        }

        public static function topReferrers()
        {
            return DB::table('users as u')
                     ->select(
                     array(
                         'u.*',
                         DB::raw('count(s.referrer_id) as refcount')
                     )
                )
                     ->join('users as s', 'u.id', '=', 's.referrer_id')
                     ->groupBy('s.referrer_id')
                     ->orderBy('refcount', 'desc')
                     ->get();
        }
    }