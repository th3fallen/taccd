<?php
    use Illuminate\Database\Eloquent\ModelNotFoundException;

    /**
     * @author Clark Tomlinson  <fallen013@gmail.com>
     * @since 1/29/14, 12:05 PM
     * @link http://www.clarkt.com
     * @copyright Clark Tomlinson © 2014
     *
     */
    class College extends Eloquent
    {

        /**
         * @var string
         */
        protected $table = 'colleges';

        /**
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function likes()
        {
            return $this->hasMany('CollegeLikes', 'college_id', 'id');
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function state()
        {
            return $this->belongsTo('State', 'state_code', 'state_code');
        }

        /**
         * @return mixed
         */
        public function confessions()
        {
            return $this->hasMany('Confession', 'college_id', 'id')//                        ->orderBy('id', 'desc')
                        ->orderBy('created_at', 'desc')
                        ->limit(10);
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function users()
        {
            return $this->hasMany('User', 'college', 'id');
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function accounts()
        {
            return $this->hasMany('LegacyUser', 'college_id', 'id');
        }

        /**
         * @return mixed
         */
        public function confessionsCount()
        {
            return $this->hasMany('Confession', 'college_id', 'id')
                        ->orderBy('id', 'desc');
        }

        /**
         * @return mixed
         */
        public function topConfessions()
        {
            return $this->hasMany('Confession', 'college_id', 'id')
                        ->orderBy('total_likes', 'desc')
                        ->limit('5');
        }

        /**
         * @return mixed
         */
        public function getTotalStudentsAttribute()
        {
            $users = $this->users()
                          ->count();
            $accounts = $this->accounts()
                             ->count();
            return $users + $accounts;
        }

        /**
         * @return mixed
         */
        public function getTotalConfessionsAttribute()
        {
            return $this->confessionsCount()
                        ->count();
        }

        /**
         * @return mixed
         */
        public function getTotalLikesAttribute()
        {
            return $this->likes()
                        ->count();
        }

        /**
         * @param $limit
         *
         * @return array|static[]
         */
        public static function topColleges($limit)
        {
            return College::join('college_likes', 'college_id', '=', 'id')
                          ->select(DB::raw('*, COUNT(college_id) as total_likes'))
                          ->groupBy('college_id')
                          ->orderBy(DB::raw('total_likes'), 'desc')
                          ->limit($limit)
                          ->get();
        }

        /**
         * @param $slug
         *
         * @return \Illuminate\Database\Eloquent\Model|null|static
         */
        public static function findBySlug($slug)
        {
            return self::where('name', '=', ucwords(Str::slug($slug, ' ')))
                       ->firstOrFail();
        }


    }