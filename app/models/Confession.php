<?php

    /**
     * @author Clark Tomlinson  <fallen013@gmail.com>
     * @since 1/29/14, 12:05 PM
     * @link http://www.clarkt.com
     * @copyright Clark Tomlinson © 2014
     *
     */
    class Confession extends Eloquent
    {

        /**
         * @var string
         */
        protected $table = 'confessions';

        /**
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function likes()
        {
            return $this->hasMany('ConfessionLikes', 'confession_id', 'id');
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function comments()
        {
            return $this->hasMany('ConfessionComments', 'confession_id', 'id');
        }

        /**
         * @param $limit
         *
         * @return array|static[]
         */
        public static function byLikedColleges($limit)
        {
            $collegeIds = Sentry::getUser()
                                ->collegeLikes()
                                ->get(array('college_id'));

            if (empty($collegeIds->count())) {
                return $collegeIds;
            }

            $collegeIds = array_flatten($collegeIds->toArray());

            return Confession::whereIn('college_id', $collegeIds)
                             ->orderBy('id', 'desc')
                             ->orderBy('created_at', 'desc')
                             ->limit($limit)
                             ->get();
        }

        /**
         * @param $limit
         *
         * @return array|static[]
         */
        public static function topConfessions($limit)
        {
            return Confession::join('confession_likes', 'confession_id', '=', 'id')
                             ->select(DB::raw('*, COUNT(confession_id) as total_likes'))
                             ->groupBy('confession_id')
                             ->orderBy(DB::raw('total_likes'), 'desc')
                             ->limit($limit)
                             ->get();
        }

        /**
         * @return mixed
         */
        public function getTotalCommentsAttribute()
        {
            return $this->comments()
                        ->count();
        }

        /**
         * @param $value
         *
         * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|static
         */
        public function getAccountIDAttribute($value)
        {
            // Todo: Remove if statement after legacy users are converted
            if (User::find($value)) {
                return User::find($value);
            }
            return LegacyUser::find($value);
        }

        /**
         * @param $value
         *
         * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|static
         */
        public function getCollegeIDAttribute($value)
        {
            return College::find($value);
        }
    }