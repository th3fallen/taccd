<?php

    /**
     * @author Clark Tomlinson  <fallen013@gmail.com>
     * @since 1/29/14, 12:05 PM
     * @link http://www.clarkt.com
     * @copyright Clark Tomlinson © 2014
     *
     */
    class CollegeLikes extends Eloquent
    {

        protected $table = 'college_likes';

        public function likes()
        {
            return $this->belongsTo('college', 'college_id', 'id');
        }

        public function user()
        {
            return $this->belongsTo('User', 'id', 'account_id');
        }
    }