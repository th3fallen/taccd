<?php

    /**
     * @author Clark Tomlinson  <fallen013@gmail.com>
     * @since 1/29/14, 12:05 PM
     * @link http://www.clarkt.com
     * @copyright Clark Tomlinson © 2014
     *
     */
    class State extends Eloquent
    {

        protected $table = 'states';

        public function colleges()
        {
            return $this->hasMany('College', 'state_code', 'state_code');
        }

        public function getNameAttribute($value)
        {
            return ucwords($value);
        }


    }