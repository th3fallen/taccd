<?php

    use Illuminate\Auth\UserInterface;
    use Illuminate\Auth\Reminders\RemindableInterface;

    class LegacyUser extends Eloquent implements RemindableInterface
    {

        /**
         * The database table used by the model.
         *
         * @var string
         */
        protected $table = 'accounts';

        protected $attributes = array(
            'avatar_url' => 'avatars/default2.png'
        );

        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = array('password');

        /**
         * Get the unique identifier for the user.
         *
         * @return mixed
         */
        public function getAuthIdentifier()
        {
            return $this->getKey();
        }

        /**
         * Get the password for the user.
         *
         * @return string
         */
        public function getAuthPassword()
        {
            return $this->password;
        }

        public function email()
        {
            return $this->email_address;
        }

        public function getAvatarUrlAttribute($value)
        {
            if (empty($value)) {
                return 'avatars/default2.png';
            }
            return $value;
        }

        public function avatar()
        {
            if (empty($this->avatar_url)) {
                return Gravatar::src($this->email());
            }
            return image($this->avatar_url);
        }

        /**
         * Get the e-mail address where password reminders are sent.
         *
         * @return string
         */
        public function getReminderEmail()
        {
            return $this->email;
        }

    }