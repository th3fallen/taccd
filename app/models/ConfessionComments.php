<?php

    /**
     * @author Clark Tomlinson  <fallen013@gmail.com>
     * @since 1/29/14, 12:05 PM
     * @link http://www.clarkt.com
     * @copyright Clark Tomlinson © 2014
     *
     */
    class ConfessionComments extends Eloquent
    {

        protected $table = 'confession_comments';

        public function confessions()
        {

            return $this->belongsTo('confessions', 'id', 'confession_id');
        }

        public function users()
        {
            return $this->hasOne('User', 'account_id', 'account_id');
        }

        public function getAccountIDAttribute($value)
        {
            // Todo: Remove if statement after legacy users are converted
            if (User::find($value)) {
                return User::find($value);
            }
            return LegacyUser::find($value);
        }
    }