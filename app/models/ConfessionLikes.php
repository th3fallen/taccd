<?php

    /**
     * @author Clark Tomlinson  <fallen013@gmail.com>
     * @since 1/29/14, 12:05 PM
     * @link http://www.clarkt.com
     * @copyright Clark Tomlinson © 2014
     *
     */
    class ConfessionLikes extends Eloquent
    {

        protected $table = 'confession_likes';

        public function likes()
        {
            return $this->belongsTo('confessions', 'confession_id', 'id');
        }

        public function getAccountIDAttribute($value)
        {
            // Todo: Remove if statement after legacy users are converted
            if (User::find($value)) {
                return User::find($value);
            }
            return LegacyUser::find($value);
        }
    }