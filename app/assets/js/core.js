/**
 * Created By: Clark Tomlinson
 * On: 1/29/14, 11:16 AM
 * Url: http://www.clarkt.com
 * Copyright Clark Tomlinson © 2014
 *
 */
var Core = {

    init: function () {
        Foundation.libs.abide.settings.patterns.alpha_numeric = /^[a-zA-Z0-9\W\s]+$/;
        $(document).foundation();
        // House keeping on dummy links
        $('body').on('click', '[href^=#]', function (event) {
            event.preventDefault();
        });
        // These events will be fired globally across every page of the site.
        this.unbindModalsOnClose();
        this.startAuth();
        this.initConfessionModal();
        this.initRequestSchool();
    },

    startAuth: function () {
        var $body = $('body');

        // Handle opening login model
        $('.login').on('click', function () {
            App.Modals.load('Login', baseUrl + 'account/login', 'tiny');
        });

        // Handle Forgot Password Action
        $body.on('click', '.forgotPassword', function (event) {
            event.preventDefault();
            App.Modals.load('Forgot Password', baseUrl + 'account/forgotPassword', 'tiny');
        });

        // Handle opening register model
        $('.register').on('click', function () {
            App.Modals.load('Register', baseUrl + 'account/register' + App.Url.param(), 'tiny');
        });


    },
    initConfessionModal: function () {
        var $body = $('body');

        $body.on('click', '#user_settings_more', function () {
            App.Modals.load('Post a confession', baseUrl + 'confession', 'tiny');
        });
    },
    initRequestSchool: function () {
        $('.search_request_form_link').on('click', function () {
            App.Modals.load('Submit your school', baseUrl + 'discover/submitSchool', 'tiny');
        });
    },
    unbindModalsOnClose: function () {
        $(document).on('opened', '[data-reveal]', function () {
            $(this).foundation();
        });


        $(document).on('close', '[data-reveal]', function () {
            $('body').off('submit', '.reveal-modal form');
        });
    }
};