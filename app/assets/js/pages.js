/**
 * Created By: Clark Tomlinson
 * On: 1/29/14, 11:13 AM
 * Url: http://www.clarkt.com
 * Copyright Clark Tomlinson © 2014
 *
 */
var controllerUrl, baseUrl, App, Foundation;

var Pages = {

    index: function () {
        $('.registerForm').ajaxForm({
            success: function (data) {
//                App.Alerts.show(data.status, data.message);
                if (data.status == 'error') {
                    App.Modals.load('Register', baseUrl + 'account/register', 'tiny', function () {
                        // Cache selectors
                        var $modal = $(this);
                        var $registerHome = $('.registerForm');

                        // Rebind old values to new form
                        $modal.find('#username').val($registerHome.find('input[name=username]').val());
                        $modal.find('#email').val($registerHome.find('input[name=email]').val());
                        $modal.find('#password').val($registerHome.find('input[name=password]').val());
                        $modal.find('.registerForm').submit();
                    });
                }
                if (data.status === 'success') {
                    App.Alerts.show(data.status, data.message);
                    $('.registerForm')[0].reset();
                }
            }
        });
    },

    discover: function () {
        var request = false;
        var $searchBox = $('#college');

        $('.discover_load_more').on('click', function () {
            var $self = $(this);
            var $elem = $(this).parent().find('.colleges');
            if ($elem.height() === 86) {
                $elem.animate({height: $self.data('maxheight') + 'px'}, 500);
                $self.find('a').text('Show Less');
            } else {
                $elem.animate({height: '86px'}, 500);
                $self.find('a').text('Show More');
            }
        });

        $searchBox.autocomplete({
            'serviceUrl': baseUrl + 'college/lookup',
            'type': 'POST',
            'onSelect': function (suggestion) {
                window.location = baseUrl + 'college/' + App.String.slug(suggestion.value);
            }
        });

    },

    college: function () {

        var confessionTemplate = [];
        var $body = $('body');

        // Handle Taccing a school
        $('#taccd_school').on('click', function () {
            var $self = $(this);
            var data = $(this).data('id');

            $.ajax({
                type: "POST",
                dataType: "json",
                url: controllerUrl + 'likeCollege',
                data: {
                    id: data
                }
            }).done(function (data) {
                if (data.status !== 'success') {
                    App.Alerts.showModal(data.status, data.message);
                    return false;
                }
                $self.toggleClass('taccd');
                var data = $self.data('id');
                if ($self.hasClass('taccd')) {
                    $self.text('Remove Tacc');
                } else {
                    $self.text('Tacc School');
                }
            });
        });

        // Handle custom art submission
        $('.submit_cover_art').on('click', function () {
            App.Modals.load('School Artwork Request', controllerUrl + 'getArtworkRequest', 'small');
        });

        // Handle liking a confession
        $body.on('click', '.college_tacc_confession', function () {
            var $self = $(this);
            var $taccCount = parseInt($self.parentsUntil('.college_confession_element').parent().find('.likecount').text());
            $.ajax({
                type: "POST",
                dataType: "json",
                url: controllerUrl + 'likeConfession',
                data: {
                    id: $self.data('id')
                }
            }).done(function (data) {
                if (data.status !== 'success') {
                    App.Alerts.alertModal(data.status, data.message);
                    return false;
                }
                $self.toggleClass('active');

                if ($self.hasClass('active')) {
                    $self.parentsUntil('.college_confession_element').parent().find('.likecount').text($taccCount + 1);
                } else {
                    $self.parentsUntil('.college_confession_element').parent().find('.likecount').text($taccCount - 1);
                }
            });
        });

        // Handle loading of more confessions
        $('.loadmoreConfessions').on('click', function () {
            var $self = $(this);
            var pageOffset = parseInt($self.data('offset'));
            var $confessionContainer = $('.confession_wrapper');

            // Lets load us some confessions
            $.ajax({
                type: "POST",
                dataType: "json",
                url: controllerUrl + 'loadConfessions',
                data: {
                    offset: pageOffset
                }
            }).done(function (data) {
                // Load the template
                if (data.length === 0) {
                    $self.remove();
                    return false;
                }
                $.each(data, function (index, value) {
                    confessionTemplate.push(value);
                });
                $self.data('offset', pageOffset + 10);
                $confessionContainer.append(confessionTemplate);
                confessionTemplate = [];

            });
        });

        // Handle opening and closing the comments drawer
        $body.on('click', '.college_expand_confession', function () {
            $(this).parentsUntil('.college_confession_element').parent().find('.expand').trigger('expandconfession');
        });

        $('.confession_wrapper').on('click expandconfession', '.expand', function () {
            var $commentContainer = $(this).parent().parent().find('.confession_comment_container');
            var $confessionWrapper = $(this).parentsUntil('.college_confession_element').parent();

            if ($commentContainer.is(':visible')) {
                $commentContainer.slideUp(500);
                $confessionWrapper.find('.college_expand_confession').removeClass('active');
                $(this).find('a').text('Expand');
            } else {
                $confessionWrapper.find('.college_expand_confession').addClass('active');
                $commentContainer.slideDown(500);
                $(this).find('a').text('Collapse');
            }

        });

        // Handle submission of commments using enter key
        $body.on('keypress', '.commentForm textarea', function (event) {
            var $wrapper = $(this).parentsUntil('.confession_comments_element').parent();
            var $self = $(this);
            var $commentWrapper = $self.parent().parent().parent();

            if (event.which === 13) {
                if ($commentWrapper.find('.confession_comment_wrapper').length) {
                    $self.parent().parent().ajaxSubmit({
                        'success': function (data) {
                            if (typeof data.status !== 'undefined') {
                                $wrapper.parent().find('.alert-box').addClass(data.status).html(data.message).slideDown('slow');
                            }
                            setTimeout(function () {
                                $wrapper.parent().find('.alert-box').slideUp('slow',
                                    function () {
                                        $(this).removeClass(data.status);
                                    });
                            }, 5000);
                            $commentWrapper.find('.confession_comment_wrapper').append(data);
                            $self.val('');
                        }
                    });
                    return true;
                }
                $self.parent().parent().ajaxSubmit({
                    'success': function (data) {
                        if (typeof data.status !== 'undefined') {
                            $wrapper.parent().find('.alert-box').addClass(data.status).html(data.message).slideDown('slow');
                        }
                        setTimeout(function () {
                            $wrapper.parent().find('.alert-box').slideUp('slow',
                                function () {
                                    $(this).removeClass(data.status);
                                });
                        }, 5000);
                        $commentWrapper.append(data);
                    }
                });
                return false;
            }
        });

        // Handle the highlighting of the load more
        $('.loadmore').hover(function () {
            $(this).find('.loadbg').addClass('active');
            $(this).find('a').addClass('active');
        }, function () {
            $(this).find('.loadbg').removeClass('active');
            $(this).find('a').removeClass('active');
        });

        // Handle loading of more comments
        $body.on('click', '.loadmoreComments', function () {
            var $commentWrapper = $(this).parent().find('.confession_comment_wrapper');
            var $confessionContainer = $('.confession_wrapper');

            var $self = $(this);
            var pageOffset = parseInt($self.data('offset'));
            var confessionid = parseInt($self.data('confessionid'));

            // Lets load us some confessions
            $.ajax({
                type: "POST",
                dataType: "html",
                url: baseUrl + 'college/loadMoreComments',
                data: {
                    offset: pageOffset,
                    confessionID: confessionid
                }
            }).done(function (data) {
                // Load the template
                if (data.length === 0) {
                    $self.remove();
                    return false;
                }
                $self.data('offset', pageOffset + 3);
                $commentWrapper.append(data);
            });

        });
    },

    dashboard: function () {
        var $body = $('body');
        var confessionTemplate = [];

        if ($.localStorage('Taccd.dismissedCallout')) {
            $('.callout').hide();
            $('.callout').next().addClass('content_hide_top')
        }

        $body.on('click', '.panel-close', function () {
            $(this).parent().slideUp('slow');
            $.localStorage('Taccd', {dismissedCallout: true});
        });


        // Handle loading of more confessions
        $('.loadmoreConfessions').on('click', function () {
            var $self = $(this);
            var pageOffset = parseInt($self.data('offset'));
            var $confessionContainer = $('.confession_wrapper');

            // Lets load us some confessions
            $.ajax({
                type: "POST",
                dataType: "json",
                url: baseUrl + 'college/loadDashboardConfessions',
                data: {
                    offset: pageOffset
                }
            }).done(function (data) {
                // Load the template
                if (data.length === 0) {
                    $self.remove();
                    return false;
                }
                $.each(data, function (index, value) {
                    confessionTemplate.push(value);
                });
                $self.data('offset', pageOffset + 10);
                $confessionContainer.append(confessionTemplate);
                confessionTemplate = [];

            });
        });

        $body.on('click', '.college_tacc_confession', function () {
            var $wrapper = $(this).parentsUntil('.college_confession_element').parent();
            var $taccCount = parseInt($wrapper.find('.likecount').text());
            $(this).toggleClass('active');

            if ($(this).hasClass('active')) {
                $(this).parentsUntil('.college_confession_element').parent().find('.likecount').text($taccCount + 1);
            } else {
                $(this).parentsUntil('.college_confession_element').parent().find('.likecount').text($taccCount - 1);
            }
            $.ajax({
                type: "POST",
                dataType: "json",
                url: $wrapper.data('url') + '/likeConfession',
                data: {
                    id: $(this).data('id')
                }
            });
        });

        // Handle submission of commments using enter key
        $body.on('keypress', '.commentForm textarea', function (event) {
            var $wrapper = $(this).parentsUntil('.confession_comments_element').parent();
            var $self = $(this);
            var $commentWrapper = $self.parent().parent().parent();

            if (event.which === 13) {
                if ($commentWrapper.find('.confession_comment_wrapper').length) {
                    $self.parent().parent().ajaxSubmit({
                        'success': function (data) {
                            if (typeof data.status !== 'undefined') {
                                $wrapper.parent().find('.alert-box').addClass(data.status).html(data.message).slideDown('slow');
                            }
                            setTimeout(function () {
                                $wrapper.parent().find('.alert-box').slideUp('slow',
                                    function () {
                                        $(this).removeClass(data.status);
                                    });
                            }, 5000);
                            $commentWrapper.find('.confession_comment_wrapper').append(data);
                            $self.val('');
                        }
                    });
                    return true;
                }
                $self.parent().parent().ajaxSubmit({
                    'success': function (data) {
                        if (typeof data.status !== 'undefined') {
                            $wrapper.parent().find('.alert-box').addClass(data.status).html(data.message).slideDown('slow');
                        }
                        setTimeout(function () {
                            $wrapper.parent().find('.alert-box').slideUp('slow',
                                function () {
                                    $(this).removeClass(data.status);
                                });
                        }, 5000);
                        $commentWrapper.append(data);
                    }
                });
                return false;
            }
        });

        // Handle opening and closing the comments drawer
        $body.on('click', '.college_expand_confession', function () {
            $(this).parentsUntil('.college_confession_element').parent().find('.expand').trigger('expandconfession');
        });

        $('.confession_wrapper').on('click expandconfession', '.expand', function () {
            var $commentContainer = $(this).parent().parent().find('.confession_comment_container');
            var $confessionWrapper = $(this).parentsUntil('.college_confession_element').parent();

            if ($commentContainer.is(':visible')) {
                $commentContainer.slideUp(500);
                $confessionWrapper.find('.college_expand_confession').removeClass('active');
                $(this).find('a').text('Expand');
            } else {
                $confessionWrapper.find('.college_expand_confession').addClass('active');
                $commentContainer.slideDown(500);
                $(this).find('a').text('Collapse');
            }

        });

        // Handle the highlighting of the load more
        $('.loadmore').hover(function () {
            $(this).find('.loadbg').addClass('active');
            $(this).find('a').addClass('active');
        }, function () {
            $(this).find('.loadbg').removeClass('active');
            $(this).find('a').removeClass('active');
        });

        // Handle loading of more comments
        $body.on('click', '.loadmoreComments', function () {
            var $commentWrapper = $(this).parent().find('.confession_comment_wrapper');
            var $confessionContainer = $('.confession_wrapper');

            var $self = $(this);
            var pageOffset = parseInt($self.data('offset'));
            var confessionid = parseInt($self.data('confessionid'));

            // Lets load us some confessions
            $.ajax({
                type: "POST",
                dataType: "html",
                url: baseUrl + 'college/loadMoreComments',
                data: {
                    offset: pageOffset,
                    confessionID: confessionid
                }
            }).done(function (data) {
                // Load the template
                if (data.length === 0) {
                    $self.remove();
                    return false;
                }
                $self.data('offset', pageOffset + 3);
                $commentWrapper.append(data);
            });

        });
    },

    account: {
        preferences: function () {

            $('#college').autocomplete({
                'serviceUrl': baseUrl + 'college/lookup',
                'type': 'POST',
            });

            //Handle showing the upload field if user chooses custom avatar over gravatar
            $('.account-page #accountInformation').on('click', 'input:checkbox', function (event) {
                var $parent = $(this).parent();

                if ($(this).attr('id') === 'customAvatar') {
                    $('.custom-avatar').slideToggle('slow');
                } else {
                    $('.custom-avatar').slideUp('slow');
                }
                $parent.find('input:checked').not($(this)).prop('checked', false);
            });

            // Confirm that user wants to close account
            $('.account-page #closeAccount').on('click', 'input[type=submit]', function (event) {
                event.preventDefault();
                event.stopPropagation();
                if (App.Alerts.confirm('deactivate', 'account')) {
                    $(this).submit();
                }
            });

            // Handle all form submissions in one nifty function

            $('.account-page FORM').on('submit', Foundation.utils.debounce(function (event) {
                event.preventDefault();

                $(this).ajaxSubmit({
                    success: function (data) {
                        App.Alerts.show(data.status, data.message, 'refresh');

                    }
                });
            }, 300, true));
        }
    }

};