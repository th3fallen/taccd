/**
 * Created By: Clark Tomlinson
 * On: 1/29/14, 10:17 AM
 * Url: http://www.clarkt.com
 * Copyright Clark Tomlinson © 2014
 *
 */
(function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-44189110-1', 'taccd.com');
ga('send', 'pageview');