var id, val, controllerUrl;


var App = {
    Alerts: {
        show: function (type, message, redirect) {
            var $alert = $('.alert-box');

            if (type === 'error') {
                type = '';
            }
            // Lets check if we should redirect anywhere
            if (typeof redirect !== "undefined") {
                if (redirect === 'refresh') {
                    $alert.addClass(type).html(message).slideDown('slow');

                    if (typeof(message) === 'object') {
                        $alert.html('<ul></ul>');
                        $.each(message, function (k, v) {
                            $alert.find('ul').append('<li>' + v + '</li>');
                            $alert.slideDown('slow');
                            $('input[name="' + k + '"]').addClass('error');
                        });
                    }
                    setTimeout(function () {
                        $alert.slideUp('slow',
                            function () {
                                $(this).removeClass(type);
                                location.reload();
                            });
                    }, 5000);
                    return true;
                }
                window.location = redirect;
                return true;
            }

            $alert.addClass(type).html(message).slideDown('slow');

            if (typeof(message) === 'object') {
                $alert.html('<ul></ul>');
                $.each(message, function (k, v) {
                    $alert.find('ul').append('<li>' + v + '</li>');
                    $alert.slideDown('slow');
                    $('input[name="' + k + '"]').addClass('error');
                });
            }
            setTimeout(function () {
                $alert.slideUp('slow',
                    function () {
                        $(this).removeClass(type);
                    });
            }, 5000);
        },
        alertModal: function (type, message) {
            var $alert = $('.reveal-modal').find('.alert-box');

            if (type === 'error') {
                type = '';
            }
            // Lets check if we should redirect anywhere
            if (typeof redirect !== "undefined") {
                if (redirect == 'refresh') {
                    $alert.addClass(type).html(message).slideDown('slow');

                    if (typeof(message) === 'object') {
                        $alert.html('<ul></ul>');
                        $.each(message, function (k, v) {
                            $alert.find('ul').append('<li>' + v + '</li>');
                            $alert.slideDown('slow');
                            $('input[name="' + k + '"]').addClass('error');
                        });
                    }
                    setTimeout(function () {
                        location.reload();
                    }, 2500);
                    return true;
                }
                $alert.addClass(type).html(message).slideDown('slow');

                if (typeof(message) === 'object') {
                    $alert.html('<ul></ul>');
                    $.each(message, function (k, v) {
                        $alert.find('ul').append('<li>' + v + '</li>');
                        $alert.slideDown('slow');
                        $('input[name="' + k + '"]').addClass('error');
                    });
                }
                setTimeout(function () {
                    window.location = redirect;
                }, 2500);
                return true;
            }

            $alert.addClass(type).html(message).slideDown('slow');

            if (typeof(message) === 'object') {
                $alert.html('<ul></ul>');
                $.each(message, function (k, v) {
                    $alert.find('ul').append('<li>' + v + '</li>');
                    $alert.slideDown('slow');
                    $('input[name="' + k + '"]').addClass('error');
                });
            }
            setTimeout(function () {
                $alert.slideUp('slow',
                    function () {
                        $(this).removeClass(type);
                    });
                App.Modals.close();
            }, 5000);
            App.Modals.open('tiny');
        },
        showModal: function (type, message, redirect) {
            var $alert = $('.reveal-modal').find('.alert-box');

            if (type === 'error') {
                type = '';
            }
            // Lets check if we should redirect anywhere
            if (typeof redirect !== "undefined") {
                if (redirect == 'refresh') {
                    $alert.addClass(type).html(message).slideDown('slow');

                    if (typeof(message) === 'object') {
                        $alert.html('<ul></ul>');
                        $.each(message, function (k, v) {
                            $alert.find('ul').append('<li>' + v + '</li>');
                            $alert.slideDown('slow');
                            $('input[name="' + k + '"]').addClass('error');
                        });
                    }
                    setTimeout(function () {
                        location.reload();
                    }, 2500);
                    return true;
                }
                $alert.addClass(type).html(message).slideDown('slow');

                if (typeof(message) === 'object') {
                    $alert.html('<ul></ul>');
                    $.each(message, function (k, v) {
                        $alert.find('ul').append('<li>' + v + '</li>');
                        $alert.slideDown('slow');
                        $('input[name="' + k + '"]').addClass('error');
                    });
                }
                setTimeout(function () {
                    window.location = redirect;
                }, 2500);
                return true;
            }

            $alert.addClass(type).html(message).slideDown('slow');

            if (typeof(message) === 'object') {
                $alert.html('<ul></ul>');
                $.each(message, function (k, v) {
                    $alert.find('ul').append('<li>' + v + '</li>');
                    $alert.slideDown('slow');
                    $('input[name="' + k + '"]').addClass('error');
                });
            }
            setTimeout(function () {
                $alert.slideUp('slow',
                    function () {
                        $(this).removeClass(type);
                    });
            }, 5000);
        },
        confirm: function (action, item, callback) {
            if (confirm("Are you sure you want to " + action + " this " + item + "?")) {
                if (typeof callback === 'function') {
                    callback();
                    return true;
                }
                return true;
            }
            return false;
        }
    },
    Modals: {
        open: function (modalSize) {
            var $modal = $('.reveal-modal');
            var size = 'medium';
            if (typeof modelSize !== 'undefined') {
                size = modelSize;
            }
            $modal.addClass(size);
            $modal.foundation('reveal', 'open');

        },
        load: function (title, contentUrl, modelSize, callback) {
            var $modal = $('.reveal-modal');
            var size = 'medium';
            if (typeof modelSize !== 'undefined') {
                size = modelSize;
            }

            $modal.find('h2').text(title);
            $modal.find('.content').load(contentUrl, function () {
                App.Modals.bindModal('submit');
                if (typeof callback == "function") {
                    $modal.promise().done(callback);
                }
            });
            $modal.addClass(size);
            $modal.foundation('reveal', 'open');
        },
        openLocal: function (title, body) {
            var $modal = $('.reveal-modal');

            $modal.find('.modal-header').text(title);
            $modal.find('.modal-body').html(body);
            $modal.foundation('reveal', 'open');
        },
        close: function () {
            var $modal = $('.reveal-modal');

            $modal.find('.modal-header').text('');
            $modal.find('.modal-body').html('');
            $modal.foundation('reveal', 'close');
            $modal.removeClass().addClass('reveal-modal')
        },
        bindModal: function (event, success, failure) {
            $('body').on(event, '.reveal-modal form', Foundation.utils.debounce(function (event) {
                event.preventDefault();

                $(this).ajaxSubmit({
                    success: function (data) {
                        App.Alerts.showModal(data.status, data.message, data.redirect);
                        if (typeof success !== 'undefined') {
                            success();
                        }
                    },
                    error: function (data) {
                        if (typeof failure !== 'undefined') {
                            failure();
                        }
                    }
                });

            }, 700, true));
        }
    },
    Data: {
        load: function (action, element) {
            $(element).load(action).fadeIn("slow");
        }
    },
    String: {
        size: function (input) {
            return input ? input.length : 0;
        },

        downcase: function (input) {
            return typeof input === 'string' ? input.toLowerCase() : input;
        },

        upcase: function (input) {
            return typeof input === 'string' ? input.toUpperCase() : input;
        },

        capitalize: function (input) {
            return typeof input === 'string' ? input.charAt(0).toUpperCase() + input.slice(1) : input;
        },

        escape: function (input) {
            return escape(input);
        },

        slug: function (input) {
            if (!input || typeof input !== 'string') return input;
            return input.replace(/^\s+|\s+$/g, '').toLowerCase().replace(/[^a-z0-9 -]/g, '').replace(/\s+/g, '-').replace(/-+/g, '-');
        }
    }, Url: {
        param: function () {
            return window.location.search;
        }
    }
};